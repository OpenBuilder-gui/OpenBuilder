![Scheme](doc/wall.png)


#OpenBuilder

#######################################################################################################

# How to Use

```
python builder.py -i input.txt -o output_directory
```


# Installation

The dependencies can be installed by using the following two methods:

##A) Through Anaconda Cloud:

Anaconda can be downloaded directly from its web page:
https://www.continuum.io/downloads


After the installation of Anaconda, it is required to install the below dependencies:
```
conda install -c openbabel openbabel=2.4.1
conda install numpy=1.11.3
conda install ply
conda install -c omnia parmed 
conda install -c omnia openmm
git clone https://github.com/shirtsgroup/InterMol.git
cd InterMol
pip install .
```




#######################################################################################################


##B) Manual installation:

For manual installation, the users are required to install the following dependencies to run
OpenBuilder tool. We expect the users to have python 3 installed (the OpenBuilder tool was tested with
python 3.6).
```
openbabel 2.4.1 or greater
	-libpng-1.6.28 or greater
	-pixman-0.34.0 or greater
	-cairo-1.14.8 or greater
	-zlib-1.2.11 or greater
	-swig-3.0.12 or greater
	-Eigen 3.3.3 or greater

Numpy-1.11.3
PLY
OpenMM
Parmed
InterMol
```

Please see the REFERENCES.txt file for their relative web pages. We are also providing a
compressed file ("*OpenBuilder_dependencies.tar.gz*") were all the dependencies are included and a
"*install_via_sources_py3.6.sh*" script that will install dependencies based on the users operating
system. 


No modifications have been made on those packages neither on the code nor on the license, 
they are provided "as-is". Please see the REFERENCES.txt file for their relative web pages and licenses.
Following the successful installation of the dependencies, the users are ready to use OpenBuilder tool.

##OpenBuilder free software:

The OpenBuilder is a free software package and can be redistributed and/or modified under the terms of 
the GNU General Public License as published by the Free Software Foundation, version 3 of the License.  
For more details see https://www.gnu.org/licenses/gpl-3.0.en.html

The OpenBuilder source code can be download from 
https://bitbucket.org/biophys-uh/OpenBuilder.git 


For more information on our research activities, please visit our webpage:
https://bitbucket.org/biophys-uh/OpenBuilder/issues?status=new&status=open

##Disclaimer:

The installation scripts provided in the OpenBuilder package are 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages arising from the use of this
software. The OpenBuilder tool and the manual will be updated if any additional features are added to
the tool and the users are requested to always use the latest version of the tool. Any comments or
issues in the usage of the tool can be posted by creating an issue in OpenBuilder public repository:

https://bitbucket.org/biophys-uh/OpenBuilder/issues?status=new&status=open