"""
The lipid_builder.py is the actual builder for the 3D structure.
It uses ``openbabel`` for concatenating pieces of molecules in one and extract its connectivity
"""

import re
import os
import sys
import os.path
import glob
import numpy as np
from random import shuffle
import openbabel as ob
import pybel
import ply.yacc as yacc

from pytools.common import common_tools
# Get the token map from the lexer.  This is required.
from parslexer.lipid import lipid_lexer

#Imports development
import time


class lipid_builder():
	"""
	"""
	def __init__ (self): 

		self.ff = ff
		self.shuffling = shuffling 
		self.SMILES = SMILES
		self.tokens = tokens 
		self.parser = parser 
		self.forcefield = forcefield
		self.string = string
		self.split = split
		self.beadsNumber  = beadsNumber 
		self.tails = tails
		self.newList  = newList 
		self.result = result
		self.corePATH = corePATH
		self.OBMol_coords = OBMol_coords 
		self.residueList = residueList   
		self.mol = mol 
		self.conv = conv 
		self.builder = builder 


	def inputReader(inputFile, outputDIR):

		"""
		It reads the user input file and looks for and stores the following options:

		* ``Forcefield``
		* ``Shuffle``
		* ``MOL_NAME``
		* ``PDB_INPUT_PATH``
		* ``TOP_INPUT_PATH``
		* ``INSERTION_METHOD``
		* Parser/lexer Expressions

		Each line starting with ``#`` will be ignored.

		"""

		SMILES_manual = []
		SMILES_automatic = []

		a1 = 0
		a2 = 0
		a3 = 0
		a4 = 0
		a5 = 0

		a1_p = 0
		a2_p = 0
		a3_p = 0
		a4_p = 0
		a5_p = 0
		
		smile_a1 = []
		smile_a2 = []
		smile_a3 = []
		smile_a4 = []
		smile_a5 = []

		AtomSites = []

		with open (inputFile,"r") as input:
			for line in input:
				if line.startswith("#") : pass
				elif line.startswith("Forcefield") :

					if re.match(r"(.*)(?<=)\s*CHARMM36", line):
						ff = "CHARMM36"
						print ("CHARMM36 forcefield loaded")

					elif re.match(r"(.*)(?<=)\s*OPLSAA2", line):
						ff = "OPLSAA2"
						print ("OPLSAA2 forcefield loaded")

					elif re.match(r"(.*)(?<=)\s*GROMOS54a7", line):
						ff = "GROMOS54a7"
						print ("GROMOS54a7 forcefield loaded")

					elif re.match(r"(.*)(?<=)\s*Slipids", line):
						ff = "Slipids"
						print ("Slipids forcefield loaded")

					elif re.match(r"(.*)(?<=)\s*Martini", line):
						ff = "Martini"
						print ("Martini forcefield loaded")

					elif re.match(r"(.*)(?<=)\s*no", line):
						ff = "no"
						print ("No forcefield selected")

					else : print("Forcefield not found")
				
				elif line.startswith("Shuffle:") :
					if re.match(r"(.*)(?<=)\s*yes", line):
						shuffling = "yes"
						print ("Shuffle ON:\nThe chains will be pseudo-randomly placed on the indicated attached sites.")
					elif re.match(r"(.*)(?<=)\s*no", line):
						shuffling = "no"
						print ("Shuffle OFF")

				elif line.startswith("MOL_NAME:") :
					molName = re.match(r"(.*)(?<=)\s", line)
					molName = molName[0].split(":")[1].strip()


				elif re.match(r"^\[[1-9][0-9]*\]", line):

					SMILES_manual.append(lipid_builder.str2smiles(ff,line)[0])
					AtomSites.append(lipid_builder.str2smiles(ff,line)[1])

				elif line.startswith("PDB_INPUT_PATH:") :
						corePATH = re.findall(r"(?<=PDB_INPUT_PATH\:).*pdb",line)
						corePATH = str(corePATH[0]).lstrip()
						print (corePATH)
						#corePATH = str(corePATH ).replace("[","").replace("]","").replace("'","").replace("\t","")
						#print ("PDB core structure in "+ str(corePATH) + " is loaded")

				elif line.startswith("TOP_INPUT_PATH:") :
						headTOP = re.findall(r"(?<=TOP_INPUT_PATH\:).*top",line)
						headTOP = str(headTOP[0]).lstrip()
						print (headTOP)


				elif line.startswith("INSERTION_METHOD:") :
					if re.match(r"(.*)(?<=)\s*manual", line):
						insertion_method = "manual"
						print ("manual insertion method loaded")

					elif re.match(r"(.*)(?<=)\s*automatic", line):
						insertion_method = "automatic"
						print ("automatic insertion method loaded")

				elif re.match(r"^\[[a][1-5]\]", line):

					if re.match(r"^\[[a][1]\]", line):
						smile_a1 = line

					elif re.match(r"^\[[a][2]\]", line):
						smile_a2 = line

					elif re.match(r"^\[[a][3]\]", line):
						smile_a3 = line

					elif re.match(r"^\[[a][4]\]", line):
						smile_a4 = line

					elif re.match(r"^\[[a][5]\]", line):
						smile_a5 = line


				elif re.match(r"^[a][1-5]\=", line):
					A1 = re.compile(r"^[a][1]\=(.*)")
					A2 = re.compile(r"^[a][2]\=(.*)")
					A3 = re.compile(r"^[a][3]\=(.*)")
					A4 = re.compile(r"^[a][4]\=(.*)")
					A5 = re.compile(r"^[a][5]\=(.*)")


					if re.match(A1, line):
						a1 = int(re.match(A1,line)[1].strip("%"))
						a1_p = a1

					elif re.match(A2, line):
						a2 = int(re.match(A2,line)[1].strip("%"))
						a2_p = a2

					elif re.match(A3, line):
						a3 = int(re.match(A3,line)[1].strip("%"))
						a3_p = a3

					elif re.match(A4, line):
						a4 = int(re.match(A4,line)[1].strip("%"))
						a4_p = a4

					elif re.match(A5, line):
						a5 = int(re.match(A5,line)[1].strip("%"))
						a5_p = a5



		
		SMILES = SMILES_manual

		SMILES = np.array(SMILES)	
		print (str(len(SMILES)) + " chains will be build")
		np.savetxt(outputDIR+"/"+"smiles.smi", SMILES, fmt="%s")

		AtomSites = np.array (AtomSites)	

		shuffling="no"
		insertion_method="manual"

		lipid_builder.builder(SMILES , shuffling, corePATH, 
						 AtomSites, ff, insertion_method, molName, headTOP, outputDIR )


		return ff, shuffling, SMILES, AtomSites, insertion_method, molName, headTOP, outputDIR



	def builder(smiles, shuffling, corePATH, AtomSites , forcefield, 
				InsertionMethod, molName, headTOP, outputDIR):


		"""
        It is the actual builder of the final 3D Structure.
        If the ``shuffle`` option is set to ``yes``, the order of the SMILE strings will be randomized.
        
        It first loads the PDB template as an openbabel object and calculates the center of geometry of it.
        Secondly, it calls the ``common_tools.SMILEtoOBLIST`` function to convert the SMILE strings in openbabel objects.
        Each openbabel object (SMILE) is attached to the attaching-site of the template.
        A new bond is created between the template and SMILE.
        An openbabel minimization follows it for relaxing 3D structure and avoid crashes.
        Finally, it generates bonds, pairs, angles, dihedrals list from the final 3D structure.

		"""
		start_time = time.time()    #timeit
		
		# quench output of non-conforming pdb-format
		ob.obErrorLog.SetOutputLevel(-1)
		
		
		
		## START Reading smile file with side chains:
		
		# Shufle position side chains if needed
		if shuffling == "yes":
			shuffle (smiles)
			np.savetxt("smile_shuffled.smi", np.array(smiles), fmt='%s')
		else : pass
				
		## END      
	
		## Initiating OBmol
		
		## START Calculationing coordinates of Center of Geometry of core:
		
		OB_CORE = []
		OB_CORE = next(pybel.readfile("pdb", corePATH)).OBMol
		OB_CORE_coords = common_tools.getCoordinatesFROMobmol(OB_CORE)
		COG  = np.mean(OB_CORE_coords, axis=0)
		print("The Geometrical Center of the Molecule is " + str(COG))
		#
		### END
		#
		## From smileStrings2OBlist:
		OB_LIST = common_tools.SMILEtoOBLIST(smiles,int(len(smiles)))
		#        
		#
		### Append side chains to OBmol
		#    
		## Starting the main loop over the side chains:
		nchains = len(smiles)
		conv = ob.OBConversion()
		Slist = AtomSites
		Slist = np.array(Slist)
		siteCoords = []
		for atom in ob.OBMolAtomIter(OB_CORE):
			if int(atom.GetId() +1) in  Slist :
				siteCoords.append((atom.GetX(),atom.GetY(),atom.GetZ()))
		siteCoords = np.array(siteCoords)
		SkeletronAtomNumbers = OB_CORE.NumAtoms()
		
		OB_CORE_NumAtoms = OB_CORE.NumAtoms()

		for chain in range(nchains):
			# Get x,y,z coordinates from OBMol Objects
			OB_coords = common_tools.getCoordinatesFROMobmol(OB_LIST[chain])

			# Define the vector from COG to the attacched sites.
			siteVector = siteCoords[chain] - COG 
			# Rotate side chain to match opitmal attaching direction -using rotational matrix
			# and translate side chain to a posisition close to the attaching site.

			common_tools.Rototranslation(OB_LIST[chain], siteVector , siteCoords[chain]*0.95)

			# Delete the 1st atom if it is Sulfur:
			# The 1st atom is transformed always in a Sulfur atom 
			# to generate a straight chain.
			lipid_builder.delete1stIsSulfur(OB_LIST[chain])
			# Append to OBmol NP structure
			NumAtom = OB_CORE.NumAtoms()
			OB_CORE += OB_LIST[chain]
			# Add missing bond between : attacched site - side chain
			OB_CORE.AddBond(int(Slist[chain]),NumAtom+1,1)
			
			
	    

		# OpenBabel Minimization:
		print ("OpenBabel Minimization")
		constraints = ob.OBFFConstraints()
		#constraints.AddDistanceConstraint(1, 10, 3.4)       # Angstroms
		#constraints.AddAngleConstraint(1, 2, 3, 120.0)      # Degrees
		#constraints.AddTorsionConstraint(1, 2, 3, 4, 180.0) # Degrees		

		for i in range(1,OB_CORE_NumAtoms+1):
			print ("Adding position restraint on atom number "+str(i) )
			constraints.AddAtomConstraint(i)

		print ("OpenBabel Minimization...")
		ob_ff = ob.OBForceField.FindForceField("MMFF94")
		ob_ff.Setup(OB_CORE, constraints)
		conv.WriteFile(OB_CORE,outputDIR+"/"+str(molName)+"_before_obmin.pdb")

		
		ob_ff.ConjugateGradients(500)
		ob_ff.GetCoordinates(OB_CORE)
		
		# Output functionalized NP structure (pdb)
		conv.WriteFile(OB_CORE,outputDIR+"/"+str(molName)+".pdb")
		conv.WriteFile(OB_CORE,outputDIR+"/"+str(molName)+"_after_obmin.pdb")

		
		# Get index lists
		# Define Bonds
		bonds    = common_tools.getBonds(OB_CORE)
		# Define angles
		angles   = common_tools.getAngles(OB_CORE)
		# Define Dihedrals
		torsions = common_tools.getTorsions(OB_CORE)
		# TODO:  Define Impropers

		# Get 1-4 pair list
		pairs14 = common_tools.getPairs(torsions, angles, bonds)
		   
		   # return the lists
		parList = common_tools.genLists(bonds, pairs14, angles, torsions)  



		if forcefield == "CHARMM36":
			from forcefields.lipid.charmm36.charmm36 import CHARMM36
			CHARMM36.lipid_ff (smiles, SkeletronAtomNumbers, parList, molName, headTOP, outputDIR, OB_CORE_NumAtoms)

		elif forcefield == "OPLSAA2":
			from forcefields.lipid.oplsaa2.oplsaa2 import OPLSAA2
			OPLSAA2.lipid_ff (smiles, SkeletronAtomNumbers, parList, molName, headTOP, outputDIR, OB_CORE_NumAtoms)


		 
		return print("--- %s seconds ---" % int((time.time() - start_time))) #print timeit


	def  str2smiles(forcefield, string):
			"""

			Here, the parser/lexer expressions are defined and heandler about the forcefield type.

			* In case of ``united atoms`` forcefield, the hydrogens are removed from the SMILE.
			* In case of ``Martini`` forcefield, ``4 TAIL units == 1 Martini bead``.

			Currently only three type of expressions are implemented (see ``lipid_builder.py`` for their implementation):

    		1. SITE SEPARATE HEAD e.g, ``[10]-CH3``
    		2. SITE SEPARATE MULTIPLE TAIL SEPARATE HEAD e.g, ``[10]-(15)CH2-CH3``
    		3. SITE SEPARATE MULTIPLE TAIL SEPARATE DOUBLEBOND SEPARATE MULTIPLE TAIL SEPARATE HEAD e.g, ``[10]-(7)CH2-cC=C-(7)CH2-CH3``.
			"""
			
			tokens = lipid_lexer.tokens
			
			# If Insertion Method is set to manual:
			def p_expression_1(p):
				"expression : SITE SEPARATE MULTIPL TAIL SEPARATE HEAD"

				# if N(CH3)3 Group is selected, adapt it to SMILE grammar

				if p[6] == str("mN(CH3)3"):
					p[6] = str("[CH2][N@SP3]([CH3])([CH3])([CH3])")
					p[0] = (p[1] + p[2] + str("[S]") + p[2] + (int((''.join(c for c in p[3] if c not in '()')))-1) * str("["+p[4]+"]"+p[5]) 
					+ str("["+p[4]+"]") + p[5] + str(p[6]))

				elif p[6] == str("COOH"):
					p[6] = str("[C@SP2]([O])([OH])")
					p[0] = (p[1] + p[2] + str("[S]") + p[2] + (int((''.join(c for c in p[3] if c not in '()')))-1) * str("["+p[4]+"]"+p[5]) 
					+ str("["+p[4]+"]") + p[5] + str(p[6]))

				elif p[6] == str("COO"):
					p[6] = str("[C@SP2]([O])([O])")
					p[0] = (p[1] + p[2] + str("[S]") + p[2] + (int((''.join(c for c in p[3] if c not in '()')))-1) * str("["+p[4]+"]"+p[5]) 
					+ str("["+p[4]+"]") + p[5] + str(p[6]))

				else:
					p[0] = (p[1] + p[2] + str("[S]") + p[2]+ int(''.join(c for c in p[3] if c not in '()')) * str("["+p[4]+"]" 
						+ p[5]) + str("["+p[6]+"]"))
			
			def p_expression_2(p):
				"expression : SITE SEPARATE HEAD"


				if p[3] == str("mN(CH3)3"):
					p[3] = str("[CH2][N@SP3]([CH3])([CH3])([CH3])")
					p[0] = (p[1] + p[2] + str("[S]") + p[2] + str(p[3]))

				elif p[3] == str("COOH"):
					p[3] = str("[C@SP2]([O])([OH])")
					p[0] = (p[1] + p[2] + str("[S]") + p[2] + str(p[3]))

				elif p[3] == str("COO"):
					p[3] = str("[C@SP2]([O])([O])")
					p[0] = (p[1] + p[2] + str("[S]") + p[2] + str(p[3]))


				else:
					p[0] = (p[1] + p[2] + str("[S]") + p[2]+ str("["+p[3]+"]"))



			def p_expression_3(p):
				"expression : SITE SEPARATE MULTIPL TAIL SEPARATE DOUBLEBOND SEPARATE MULTIPL TAIL SEPARATE HEAD"

				if  p[6] == str("cC=C"):
					p[6] = str("/[CH]=[CH]\\")
					p[0] =  (p[1] + p[2] + str("[S]") + p[2]+ int(''.join(c for c in p[3] if c not in '()')) * str("["+p[4]+"]" 
							+ p[5]) + str(p[6]) + p[7]+
							+ int(''.join(c for c in p[8] if c not in '()')) * str("["+p[9]+"]" 
							+ p[10]) + str("["+p[11]+"]"))


			# If Insertion Method is set to atomatic:

			# Error rule for syntax errors
			def p_error(p):
				print("Syntax error in input!")
			
			# Build the parser
			parser = yacc.yacc()
			
			forcefield_dict={
			"CHARMM36"		:	"atomistic",
			"OPLSAA2"			:"atomistic",
			"Slipids"		:	"atomistic",
			"GROMOS54a7"	:	"united_atom",
			"Martini"		:	"Martini"
			}
			
			
			result = parser.parse(string)
			
			if forcefield_dict[forcefield] == "atomistic" :
				SiteNumber = (int (re.findall(r"\d+",result)[0]) )
				result = re.sub(r"^\[\d+\]\-", "", result) 

			elif forcefield_dict[forcefield]  == "united_atom" :
				SiteNumber = (int (re.findall(r"\d+",result)[0]) )
				result = re.sub(r"^\[\d+\]\-", "", result) 
				result = re.sub(r"H[0-9]|H","",result)
				
			elif forcefield_dict[forcefield]   == "Martini":
				# Dictionary for the head groups:
				# each terminal group is named differently
				# if the atomtype is different in the Martini forcefield
				headGroups = {
				"[CH3]"   								: 	["[C]","C1" ],
				"[C@SP2]([O])([O])"  	 				: 	["[O]","Qa" ],
				"[C@SP2]([O])([OH])"  					: 	["[F]","Nda"],
				"[NH3]"   								: 	["[N]","Qd" ],
				"[NH2]"   								: 	["[F]","Nda"],
				"[CH2][N@SP3]([CH3])([CH3])([CH3])" 	:	["[B]","Q0"]
				
				}

				# The SMILE string is split into single groups:
				# The SITE and the HEAD groups are removed from the counting.
				# The total number of groups is divided by 4 to follow the MARTINI 
				# rules for CH2 lipid tails.

				# The CG bead Number for the chains is rounded.
				SiteNumber = (int (re.findall(r"\d+",result)[0]) )
				result = re.sub(r"^\[\d+\]\-", "", result)
				split = result.split("-")
				#print(split[-1])
				beadsNumber  = round(((len(split)-2)/4))
				tails = int(beadsNumber) * "[C] "
				tails = "-".join(tails.split(" ")[:-1:])
				split[-1] = headGroups[split[-1]][0]
				newList = "-".join([split[0],  tails     ,split[-1]])      
				result =   newList 

			return result, SiteNumber
	

	def SmileList2Lists(smilesList):
		"""
		It reads a SMILE list and convert it in  a list of len(smiles) lists.
		Furtheremore, It replaces the attched site number [ATTACHED_SITE] to [S] and
		Convert back the new list of len(smiles) lists to 1 list of 
		len(smile) elements.

		"""
	
		splitted = []
		for line in smilesList:
			splitted.append(line.split("-"))
		smileLists = []
		NewSmiles  = []
		for List in range(len(splitted)):
			smileLists.append ( [item.replace('[ATTACCHED_SITE]', '[S]') for item in splitted[List]] )
			NewSmiles.append ("-".join(smileLists[List])) 	
		return NewSmiles, smileLists
	
	
	def delete1stIsSulfur(OBMol_Object):
		"""
		It deletes the first atom of the SMILES string. It is always a sulphur atom due to the substitution made by the ``SmileList2Lists`` function.
		"""
		
		import openbabel
		import pybel
		
		for atom in ob.OBMolAtomIter(OBMol_Object):
			if atom.GetIdx() == 1:
				if atom.IsSulfur():
					OBMol_Object.DeleteAtom(atom)



	def TopolReader(inputFN):
		"""
		It walks on the final 3D structure and perception the bonds, pairs, angles, and dihedral lists.
		"""

		core_atoms     = []
		core_bonds     = []
		core_pairs     = []
		core_angles    = [] 
		core_dihedrals = []
		core_impropers = []
		
		with open(inputFN,"r") as file:
			for line in file:
				if line.startswith("#"):
					continue
				if line.startswith("\n"):
					continue
				elif "ATOMS" in line:
					break
		
		
			for line in file:
				if line.startswith("#"):
					continue
				if line.startswith("\n"):
					continue
				elif "BONDS" in line:
					break
				else:
					core_atoms.append(line)
		
		
			for line in file:
				if line.startswith("#"):
					continue
				if line.startswith("\n"):
					continue
				elif "PAIRS" in line:
					break
				else:
					core_bonds.append(line)
		
		
			for line in file:
				if line.startswith("#"):
					continue
				if line.startswith("\n"):
					continue
				elif "ANGLES" in line:
					break
				else:
					core_pairs.append(line)
		
		
			for line in file:
				if line.startswith("#"):
					continue
				if line.startswith("\n"):
					continue
				elif "DIHEDRALS" in line:
					break
				else:
					core_angles.append(line)
		
		
		
			for line in file:
				if line.startswith("#"):
					continue
				if line.startswith("\n"):
					continue
				elif "IMPROPERS" in line:
					break
				else:
					core_dihedrals.append(line)
		
		
		
			for line in file:
				if line.startswith("#"):
					continue
				if line.startswith("\n"):
					continue
				else:
					core_impropers.append(line)
		
		

		if np.array(core_atoms).size:
			# atom number, atom type, atom name, charge, mass
			core_atoms     = np.stack(np.core.defchararray.split(core_atoms    ))

		if np.array(core_bonds).size:
			core_bonds     = np.stack(np.core.defchararray.split(core_bonds    ))

		if np.array(core_pairs).size:
			core_pairs     = np.stack(np.core.defchararray.split(core_pairs    ))

		if np.array(core_angles).size:
			core_angles     = np.stack(np.core.defchararray.split(core_angles    ))

		if np.array(core_dihedrals).size:
			core_dihedrals     = np.stack(np.core.defchararray.split(core_dihedrals   ))

		if np.array(core_impropers).size:
			core_impropers     = np.stack(np.core.defchararray.split(core_impropers    ))


		return core_atoms, core_bonds, core_pairs, core_angles, core_dihedrals, core_impropers