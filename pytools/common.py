"""
``common.py`` is situated inside the ``pytools`` folder.
It contains all the functions shared by all modules. \n

"""
import numpy as np
import openbabel as ob
from shutil import copyfile
import glob
import pybel
import os
import os.path

# OpenMM Imports
import simtk.openmm as mm
import simtk.openmm.app as app
from simtk.unit import *

# ParmEd Imports
from parmed import load_file, gromacs, charmm, amber, namd
from parmed import unit as u

# Intermol Imports
from intermol import gromacs, desmond, lammps


class common_tools():
	"""
 	Common Tools
	"""


	def getCoordinatesFROMobmol(OBmol_Object):
		"""
		Given an OBMol object , it will make an array 
		cointained all the x,y,z atom coordinates.
		"""

		OBMol_coords = []
		for atom in ob.OBMolAtomIter(OBmol_Object):
			OBMol_coords.append(list((atom.GetX(),atom.GetY(),atom.GetZ())))
		OBMol_coords = np.array(OBMol_coords)
		return OBMol_coords



	def getCoordinatesFROMpdb(pdbfile):	
		"""
		Given an .pdb file , it will make an array 
		cointained all the  x,y,z atom coordinates.

		"""
	
	
		ListAtoms = []
		OpenMol= next(pybel.readfile("pdb", pdbfile))
		for atom in OpenMol:
			ListAtoms.append(atom.coords)
		ListAtoms = np.array(ListAtoms) 
		return ListAtoms
	

	
	def smilesFile2pdb(smilesstring,nchains):
		"""
		Given a list of SMILE strings it will be convert in PDB 
		all the molecules written in SMILES language within it.

		"""

		# quench output of non-conforming pdb-format
		ob.obErrorLog.SetOutputLevel(-1)		
		
		# Making a new directory:
		if not os.path.exists("PDB_chains"):
			os.makedirs("PDB_chains")
		for i in range(0,nchains): 
			mol = ob.OBMol()
			conv = ob.OBConversion()
			conv.SetInAndOutFormats("smi", "pdb")
			conv.ReadString(mol,smilesstring[i])
			builder = ob.OBBuilder()
			builder.Build(mol)
			filename = "chain_"+str(i+1)
			conv.WriteFile(mol, "PDB_chains/%s.pdb" % filename)

	def SMILEtoOBLIST(smilesstring,nchains):
		"""Given a list of SMILE strings it will be convert in openbabel 
		object all the molecules written in SMILES language within it.
		
		"""
		residueList = []
		for residue in range(0,nchains): 
			mol = ob.OBMol()
			conv = ob.OBConversion()
			conv.SetInAndOutFormats("smi", "pdb")
			conv.ReadString(mol,smilesstring[residue])
			builder = ob.OBBuilder()
			builder.Build(mol)
			residueList.append(mol)
			#print(smilesstring[residue])

		return residueList

	def OriginTranslation(OBmol_Object, x_O , y_O, z_O, ):
		"""
		Given a OBmol object and the x,y,z coordinates of the new origin,
		all atoms coordinates will be translated to the new origin.

		"""
	
		T = translationVector = np.array([x_O , y_O, z_O]) - common_tools.getCoordinatesFROMobmol(OBmol_Object)[0] 
		for atom in ob.OBMolAtomIter(OBmol_Object):
			atom.SetVector(float(atom.GetX()+T[0]),float(atom.GetY()+T[1]),float(atom.GetZ()+T[2]))
	

	def unitVector(vector):
		"""
		Given a vector, the unitVector ( unitVector = (Vector / \|Vector\| ))  
		and the magnitude of it will be returned.

		"""
	
		N = vector / ( np.sqrt((vector[0] ** 2 + vector[1] ** 2 + vector[2] ** 2)))
		mag  = np.sqrt((vector[0] ** 2 + vector[1] ** 2 + vector[2] ** 2))
		return N,mag
		

	def rotation(theta, u, v, w):
		"""
		Given an angle, and x,y,z, coordinates of the destination vector, 
		the rotation matrix will be calculated based on 
		www.mathworld.wolfram.com/RodriguesRotationFormula.html 

		"""
		rot_matrix = np.zeros((3, 3))
		rot_matrix[0, 0] = np.cos(theta) + u*u*(1 - np.cos(theta))
		rot_matrix[0, 1] = u*v*(1 - np.cos(theta)) - w*np.sin(theta)
		rot_matrix[0, 2] = v*np.sin(theta) + u*w*(1 - np.cos(theta))
		rot_matrix[1, 0] = w*np.sin(theta) + u*v*(1 - np.cos(theta))
		rot_matrix[1, 1] = np.cos(theta) + v*v*(1 - np.cos(theta))
		rot_matrix[1, 2] = -u*np.sin(theta) + v*w*(1 - np.cos(theta))
		rot_matrix[2, 0] = -v*np.sin(theta) + u*w*(1 - np.cos(theta))
		rot_matrix[2, 1] = u*np.sin(theta) + v*w*(1 - np.cos(theta))
		rot_matrix[2, 2] = np.cos(theta) + w*w*(1 - np.cos(theta))
		return rot_matrix
	
	
	def Rototranslation(OBmol_Object, DestinationVector, siteCoords ):
		""" 
		Given a OBmol object, the destination vector, the new origin (array) where 
		the molecule has to be translated, a rototranslation will be computed. 

		"""
		common_tools.OriginTranslation(OBmol_Object, 0,0,0)
		
		
		# Determine the center of the line joining the atoms
		# with respect to the last atom:
	
		center = 0.5*(common_tools.getCoordinatesFROMobmol(OBmol_Object)[-1] - common_tools.getCoordinatesFROMobmol(OBmol_Object)[0])
		center = common_tools.getCoordinatesFROMobmol(OBmol_Object)[-1] + center
		newdata = np.around((common_tools.getCoordinatesFROMobmol(OBmol_Object) - center), decimals=3 )
		direction = v1 =  - DestinationVector
		axis = common_tools.unitVector(v1)[0]
	
		# Obtain the magnitude of vector connecting atom1 to origin:
		mag = common_tools.unitVector(newdata[0])[1]
	
		# Evaluate the rotation angle. Remember, axis is unit vector:
		angle = np.arccos(np.vdot(newdata[0], axis)/mag)
	
		# Obtain the rotation axis by doing cross product:
		axis_rotate = np.cross(newdata[0], axis)
	
		# Renormalize the rotation axis to unit vector:
		axis2 , mag= common_tools.unitVector(axis_rotate)
		axis_rotate = axis_rotate/mag
	
		# Evaluate the rotation matrix:
		rot = common_tools.rotation(angle, axis_rotate[0], axis_rotate[1], axis_rotate[2])
	
		# Apply rotation:
		for (idx, x) in enumerate(newdata):
			newdata[idx] = np.dot(rot, x)
	
		# Apply translation:
		newdata += center
		x,y,z = newdata[:,0], newdata[:,1],newdata[:,2]
	
	
		for idx, atom in enumerate(ob.OBMolAtomIter(OBmol_Object)):
			atom.SetVector(float(x[idx]),float(y[idx]),float(z[idx]))
	
		common_tools.OriginTranslation(OBmol_Object, siteCoords[0], siteCoords[1],siteCoords[2])


	def getAtNum(atype):
		"""
		Deduce atomic number from the first letter of atomtype name.
	
		"""
		if atype[0] == 'C':
			return 6
		elif atype[0] == 'H':
			return 1
		elif atype[0] == 'O':
			return 8
		elif atype[0] == 'N':
			return 7
		elif atype[0] == 'S':
			return 16
		elif atype[0] == 'F':
			if len(atype) == 1:
				return 9
			elif atype[1] in 'Ee':
				return 26
		elif atype == isymdu:
			return 0
		else:
			print("Error: Unrecognized element for atomtype", atype)
	
	
	def getMolAtomByName(mol, aname):
		"""
		Returns atom from molecule with specified name (IGRAPH).
	
		"""
		for i in ob.OBMolAtomIter(mol):
			if i.IGRAPH == aname:
				return i


	def addBond(mol, begin, end, bond=ob.OBBond()):
		"""
		Add bond to a molecule within the given start and end atoms.
	
		"""
		bond.SetBegin(begin)
		bond.SetEnd(end)
		return mol.AddBond(bond)
	
	
	def getResCharge(residue):
		"""
		Calculate summary charge of given residue.
	
		"""
		charge = 0
		for i in ob.OBResidueAtomIter(residue):
			charge += i.GetPartialCharge()
	
		return charge
	
	
	def getBonds(mol):
		"""
		Get list of bonds in the molecule.
	
		"""
		bonds = []
		for bond in ob.OBMolBondIter(mol):
			a = bond.GetBeginAtomIdx()
			b = bond.GetEndAtomIdx()
			s = [a, b]
			s.sort()
			bonds.append(s)
		bonds.sort()
	
		return bonds
	
	
	def getAngles(mol):
		"""
		Get list of angles in the molecule.
	
		"""
		angles = []
		for angle in ob.OBMolAngleIter(mol):
			angles.append([angle[1]+1, angle[0]+1, angle[2]+1])
		for i in angles:
			if i[2] < i[0]:
				i.reverse()
		angles.sort(key=lambda a: [a[1], a[0], a[2]])
	
		return angles
	
	
	def getTorsions(mol):
		"""
		Get list of torsions in the molecule.
	
		"""
		torsions = []
		for torsion in ob.OBMolTorsionIter(mol):
			torsions.append([torsion[0]+1, torsion[1]+1,
							torsion[2]+1, torsion[3]+1])
		for i in torsions:
			if i[2] < i[1]:
				i.reverse()
		torsions.sort(key=lambda a: [a[1], a[2], a[0], a[3]])
	
		return torsions
	
	
	def getPairs(torsions, angles, bonds):
		"""
		Get list of 1-4 pairs in the molecule.
	
		"""
		pairs14 = [sorted([i[0], i[3]]) for i in torsions]
		# Filter out duplicate pairs and those in 4 and 5 member rings
		excludel = [[j[0],j[2]] for j in angles]
		excludel.extend(bonds)
		pairs14 = common_tools.uniquify(common_tools.list_difference(pairs14, excludel))
		pairs14.sort()
	
		return pairs14	

	def genLists(bonds, pairs14, angles, torsions):
		"""
		 It generates a lsit of bonds, pairs, angles and torsions.
		"""

		BONDS = []
		for i in bonds:
			BONDS.append((i[0] , i[1]))
		BONDS = np.array(BONDS)
		
		
		PAIRS14 = []
		for i in pairs14:
			PAIRS14.append((i[0] , i[1]))
		PAIRS14 = np.array(PAIRS14)
		
		
		ANGLES = []
		for i in angles:
			ANGLES.append((i[0] , i[1], i[2]))
		ANGLES = np.array(ANGLES)
		
		DIHEDRALS = []
		for i in torsions:
			DIHEDRALS.append((i[0] , i[1], i[2], i[3]))
		DIHEDRALS = np.array(DIHEDRALS)
		
		return BONDS, PAIRS14, ANGLES, DIHEDRALS

	def uniquify(seq):
		
		"""
		Given a list, it will return list without duplicates.
		"""

		seen = set()
		return [i for i in seq if str(i) not in seen and not seen.add(str(i))]
		
		
		
	def list_difference(s1, s2):
		
		"""
		Given two lists, a list of only those elements 
		of the first list which are not in the
		second list will be returned.
		"""

		s2 = set([str(i) for i in s2])
		return [j for j in s1 if str(j) not in s2]



	#def OBminimization():
	#	""" 
	#	Openbabel minimization
	#	"""
	#
	#	conv = ob.OBConversion()
	#	conv.SetInAndOutFormats('xyz','xyz')
	#	mol = obOBMol()
	#	conv.ReadFile(mol,'my_mol.xyz')


	def OpenMMsim_XY(GRO,TOP,max_iteration, Nsteps, dt, restrains,tolerance,outputFN):
		"""It uses the gromacs topology file ``.top`` and the relative ``.gro`` file to make a simulation in vacuum
        with OpenMM. The number of steps and the integration steps are set with ``Nsteps`` and ``dt`` respectively. 
        The ``max_iteration`` option is used for making multiple cycles of minimization before running the production run.
        The ``tolerance`` parameter is used for setting the tolerance of the energy minimization.


		"""

		# Load the Gromacs files
		print('Loading Topology files...')
		top = load_file(TOP)
		gro = load_file(GRO)
		

		# Create the OpenMM system
		print('Creating OpenMM System')
		


		#system = top.createSystem(nonbondedMethod=CutoffPeriodic,
								 #nonbondedCutoff=1*nanometer, constraints=None)

		# Number of steps
		nsteps        = Nsteps # 1 nanosecond 
		time_step     = dt*picoseconds
		
		# Thermostat Setting
		temperature   = 300*kelvin
		thermostat_freq = 1/picosecond
		
		# Barostat Setting
		pressure      = 1.0*bar
		barostat_freq = 25 # default 25
		
		
		
		# System Setting
		system = top.createSystem(nonbondedMethod=app.CutoffNonPeriodic, 
									nonbondedCutoff=1*nanometer, constraints=None)
		
		## Set Harmonic Potential on Z
		force = mm.CustomExternalForce("k*((z-z0)^2)")
		force.addGlobalParameter("k", 1000.0*kilojoules_per_mole/nanometers**2)
		force.addPerParticleParameter("x0")
		force.addPerParticleParameter("y0")
		force.addPerParticleParameter("z0")
		force.setForceGroup(1)
		# Add constrains (mass == 0)
		if restrains is "no" :
			print ("No position restrains applied")
		
		else:	
			for i, atom_crd in enumerate(gro.positions):
				if i in restrains:
					force.addParticle(i, atom_crd.value_in_unit(u.nanometers))

		system.addForce(force)
		
		integrator = mm.LangevinIntegrator(temperature, 1/picosecond, 0.002*picoseconds)


		# Create the Simulation object
		sim = app.Simulation(top.topology, system, integrator, platform=None)

		# Convert Coordinates
		coordinates = gro.coordinates / 10
		sim.context.setPositions(coordinates)


		
		# Minimize the energy
		print("Minimization ...")
		sim.minimizeEnergy(tolerance=tolerance*kilojoules_per_mole,
						   maxIterations=max_iteration)

		position = sim.context.getState(getPositions=True).getPositions()

		print("Done")

		## Run the simulation
		print("Running Simulation in Vacuum for "+str(nsteps)+" steps")
		sim.step(nsteps)
		print("Done")

		# Printing output file
		position = sim.context.getState(getPositions=True).getPositions()
		app.PDBFile.writeFile(sim.topology, position,
							  open(outputFN, "w"))


	def MoleculeLoader(inputPDB):
	
		"""
		It uses ``parmed`` libraries to read the PDB/GRO input file. 
		"""

		inputStructure = load_file(inputPDB)
	
		return inputStructure
	
		
	def CoordinatesCatcher(inputStructure):

		"""
		Given a ``parmed`` object (PDB/GRO), the atom coordinates will be returned in a 3D array.
		"""
	
		# Declare the total number of atoms
		atoms_number = len(inputStructure.atoms)
		
		# Split atom coordinates in x,y,z
		# Only 3 numbers after the comma are taken in consideration
		x = [ '{:.3f}'.format(i) for i in loadfile.coordinates[:,0] ]
		y = [ '{:.3f}'.format(i) for i in loadfile.coordinates[:,1] ]
		z = [ '{:.3f}'.format(i) for i in loadfile.coordinates[:,2] ]
		
		# Concatenate x,y,z corrdinates and convert in x_y_z string
		coordinates_list = []
		for i in range(atoms_number):
			coordinates_list.append(str(x[i])+"_"+str(y[i])+"_"+str(z[i]))
		
		# Create a numpy array
		coordinates_list = np.vstack(coordinates_list)
	
		return coordinates_list
		
		
	def ResiduesCatcher(inputStructure):

		"""
		Given a ``parmed`` object (PDB/GRO), the serial atom list, the atoms list, and the residues list will be extracted.		
		"""
	
		# Declare the total number of atoms
		atoms_number = len(inputStructure.atoms)
		# Parmed places all the information of the same atom in a string
		# Regular expression are used to catch residue and atom names
		residues_list = []
		atoms_list    = []
	
		# Serial number of Atoms (From 1 to N )
		serial_list    = []
		
		for i in range (atoms_number):
			residues_list.append( str(loadfile.atoms[i]).split(";")[1].split(" In ")[1].split(" ")[0] )
			atoms_list.append( str(loadfile.atoms[i]).split(";")[0].split(" [")[0].split("<Atom ")[1])
			serial_list.append(int(i+1))
		
		# Create numpy arrays
		residues_list = np.array(residues_list)
		atoms_list = np.array(atoms_list)
		serial_list = np.array(serial_list)
	
		return serial_list, atoms_list, residues_list
		
	
	def ChainsCatcher(inputStructure):

		"""
		Given a ``parmed`` object (PDB/GRO), the chains list will be returned.		
		"""
	
		# Declare the total number of atoms
		atoms_number = len(inputStructure.atoms)	
	
		# Parmed places the "CHAIN" information inside the residue list.
		# Each residue in the "loadfile.residues" list has to be iterated 
		# for all the atoms inside each residues
	
		chains_list = []
		
		for i in loadfile.residues:
			for j in i:
				chains_list.append(str(re.findall(r"chain.(.*?);",str(i))).strip("[]\"\'"))
		
		# Create numpy arrays
		chains_list = np.array(chains_list)
		
		return chains_list

	def TopAllnotDesmond(outputDIR, inputTOP, input3D, gromacsDIR, molName):
		"""
		  It converts and prints all parameter files for all the software supported by ``parmed`` and ``InterMol`` except for ``Desmond``.  		
		"""
		topology_conversion.Gromacs2Charmm_Namd(outputDIR, inputTOP, input3D, molName)
		topology_conversion.Gromacs2Amber(outputDIR, inputTOP, input3D, molName)
		topology_conversion.Gromacs2Lammps(outputDIR, inputTOP, input3D, gromacsDIR, molName)

	def TopAll(outputDIR, inputTOP, input3D, gromacsDIR, molName):
		"""
		  It converts and prints all parameter files for all the software supported by ``parmed`` and ``InterMol``.
		"""
		topology_conversion.Gromacs2Charmm_Namd(outputDIR, inputTOP, input3D, molName)
		topology_conversion.Gromacs2Amber(outputDIR, inputTOP, input3D, molName)
		topology_conversion.Gromacs2Desmond(outputDIR, inputTOP, input3D, gromacsDIR, molName)
		topology_conversion.Gromacs2Lammps(outputDIR, inputTOP, input3D, gromacsDIR, molName)


	def TopOPLS(outputDIR, inputTOP, input3D, gromacsDIR, molName):
		"""
		It converts and prints all parameter files for ``Desmond`` and ``Lammps`` only.
		"""
		topology_conversion.Gromacs2Desmond(outputDIR, inputTOP, input3D, gromacsDIR, molName)
		topology_conversion.Gromacs2Lammps(outputDIR, inputTOP, input3D, gromacsDIR, molName)



class topology_conversion():


	"""
	Class for topology_conversion
	""" 


	def Gromacs2Charmm_Namd(outputDIR, inputTOP, input3D, molName):
		"""
		It converts the GROMACS parameter files to CHARMM (via ``parmed``) and NAMD readable files.
		""" 
		charmmDIR=outputDIR+"/"+"CHARMM/"
		os.mkdir(charmmDIR)
		top = load_file(inputTOP, xyz=input3D)
		top.save(charmmDIR+molName+".psf")
		top.save(charmmDIR+molName+".crd", format='charmmcrd')
		charmm.CharmmParameterSet.from_structure(top).write(top=charmmDIR+molName+".rtf", par=charmmDIR+"charmm.prm")
		namdDIR=outputDIR+"/"+"NAMD/"
		os.mkdir(namdDIR)
		copyfile (charmmDIR+molName+".psf", namdDIR+molName+".psf")

		# Add ! to *prm files
		for ndx, file in enumerate(glob.glob(charmmDIR+"*.prm")):
			with open(namdDIR+"namd_"+str(ndx)+".prm", 'w') as output_file:
				with open(file, 'r') as input_file:
					for line in input_file:
						if line.startswith("ATOM") or line.startswith("MASS") or ( "BOM" in line ) or ( "WRN" in line ) or line.startswith("set") or line.startswith("if"):
							line ="!"+line
						output_file.write(line)

		# Add ! to *str files
		for ndx, file in enumerate(glob.glob(charmmDIR+"*.str")):
			with open(namdDIR+"namd_"+str(ndx)+".str", 'w') as output_file:
				with open(file, 'r') as input_file:
					flag4par = 0
					flag4nbfix = 0
					for line in input_file:
						if ( "read para" in line):                
							flag4par = 1
						if ( flag4par == 1 ):
							if ( "read para" in line ):
								continue
							if line.startswith("ATOM") or line.startswith("MASS") or ( "BOM" in line ) or ( "WRN" in line ) or line.startswith("set") or line.startswith("if"):
							   line = "!" + line
							if ( "NBFix between carboxylate and sodium" in line ):
								flag4nbfix = 1
								continue
							if ( flag4nbfix == 1 ) and ( "*" in line ):
								flag4nbfix = 0
								continue

							output_file.write(line)                




	def Gromacs2Amber(outputDIR, inputTOP, input3D, molName):
		"""
		It converts the GROMACS parameter files to AMBER with ``parmed``
		""" 
		amberDIR=outputDIR+"/"+"AMBER/"
		os.mkdir(amberDIR)
		top = load_file(inputTOP, xyz=input3D)
		amb = amber.ChamberParm.from_structure(top)
		amb.write_parm(amberDIR+"amber_system.parm7")
		amb.write_rst7(amberDIR+"amber_system.rst7")


	def Gromacs2Desmond(outputDIR, inputTOP, input3D, gromacsDIR, molName):
		"""
		It converts the GROMACS parameter files to DESMOND with ``InterMol``.
		""" 
		desmondDIR=outputDIR+"/"+"DESMOND/"
		os.mkdir(desmondDIR)
		gro = load_file(input3D)
		
		if not os.path.isfile(gromacsDIR+molName+".gro"):		
			gro.save(gromacsDIR+molName+".gro")

		gromacsTOP=gromacs.load(gromacsDIR+"topol.top", gromacsDIR+molName+".gro")
		desmond.save(desmondDIR+molName+".cms", gromacsTOP)


	def Gromacs2Lammps(outputDIR, inputTOP, input3D, gromacsDIR, molName):
		"""
		It converts the GROMACS parameter files to LAMMPS with ``InterMol``.
		""" 
		
		lammpsDIR=outputDIR+"/"+"LAMMPS/"
		os.mkdir(lammpsDIR)
		gro = load_file(input3D)
		
		if not os.path.isfile(gromacsDIR+molName+".gro"):		
			gro.save(gromacsDIR+molName+".gro")

		gromacsTOP=gromacs.load(gromacsDIR+"topol.top", gromacsDIR+molName+".gro")
		lammps.save(lammpsDIR+molName+".cms", gromacsTOP)


