"""
The nanoparticle_builder.py is the actual builder for the 3D structure.
It uses ``openbabel`` for concatenating pieces of molecules in one and extract its connectivity.
"""
import re
import os
import sys
import os.path
import glob
import numpy as np
from random import shuffle
import openbabel as ob
import pybel
import ply.yacc as yacc

from pytools.common import common_tools
# Get the token map from the lexer.  This is required.
from parslexer.nanoparticle import NPlexer

#Imports development
import time




forcefield_dict={
			"CHARMM36"		:	"atomistic",
			"GROMOS54a7"	:	"united_atom",
			"MARTINI"		:	"martini"
}


class NPbuilder():
	"""
	"""

	def __init__ (self): 

		self.ff = ff
		self.shuffling = shuffling 
		self.SMILES = SMILES
		self.tokens = tokens 
		self.parser = parser 
		self.forcefield = forcefield
		self.string = string
		self.split = split
		self.beadsNumber  = beadsNumber 
		self.tails = tails
		self.newList  = newList 
		self.result = result
		self.corePATH = corePATH
		self.OBMol_coords = OBMol_coords 
		self.residueList = residueList   
		self.mol = mol 
		self.conv = conv 
		self.builder = builder 


	def inputReader(inputFile, outputDIR):

		"""
		It reads the user input file and looks for and stores the following options:

		* ``Forcefield``
		* ``Shuffle``
		* ``MOL_NAME``
		* ``PDB_INPUT_PATH``
		* ``TOP_INPUT_PATH``
		* ``INSERTION_METHOD``
		* Parser/lexer Expressions

		Each line starting with ``#`` will be ignored.

		"""

		mode = "normal"
		SMILES_manual = []
		SMILES_automatic = []

		a1 = 0
		a2 = 0
		a3 = 0
		a4 = 0
		a5 = 0

		a1_p = 0
		a2_p = 0
		a3_p = 0
		a4_p = 0
		a5_p = 0
		
		smile_a1 = []
		smile_a2 = []
		smile_a3 = []
		smile_a4 = []
		smile_a5 = []

		AtomSites = []

		with open (inputFile,"r") as input:
			for line in input:
				if line.startswith("#") : pass
				elif line.startswith("Forcefield") :

					if re.match(r"(.*)(?<=)\s*CHARMM36", line):
						ff = "CHARMM36"
						print ("CHARMM36 forcefield loaded")


					elif re.match(r"(.*)(?<=)\s*GROMOS54a7", line):
						ff = "GROMOS54a7"
						print ("GROMOS54a7 forcefield loaded")

					elif re.match(r"(.*)(?<=)\s*MARTINI", line):
						ff = "MARTINI"
						print ("Martini forcefield loaded")

					else : print("Forcefield not found")


				elif line.startswith("MOL_NAME:") :
					molName = re.match(r"(.*)(?<=)\s", line)
					molName = molName[0].split(":")[1].strip()
				

				elif line.startswith("Mode:") :
					mode = re.match(r"(.*)(?<=)\s", line)
					mode = mode[0].split(":")[1].strip()
					print (mode)

				elif line.startswith("Shuffle:") :
					if re.match(r"(.*)(?<=)\s*yes", line):
						shuffling = "yes"
						print ("Shuffle ON:\nThe chains will be pseudo-randomly placed on the indicated attached sites.")
					elif re.match(r"(.*)(?<=)\s*no", line):
						shuffling = "no"
						print ("Shuffle OFF")

				elif re.match(r"^\[[1-9][0-9]*\]", line):

					SMILES_manual.append(NPbuilder.str2smiles(ff,line)[0])
					AtomSites.append(NPbuilder.str2smiles(ff,line)[1])

				elif line.startswith("PDB_INPUT_PATH:") :
						corePATH = re.findall(r"(?<=PDB_INPUT_PATH\:).*pdb",line)
						corePATH = str(corePATH[0]).lstrip()
						print (corePATH)
						#corePATH = str(corePATH ).replace("[","").replace("]","").replace("'","").replace("\t","")
						#print ("PDB core structure in "+ str(corePATH) + " is loaded")

				elif line.startswith("TOP_INPUT_PATH:") :
						headTOP = re.findall(r"(?<=TOP_INPUT_PATH\:).*top",line)
						headTOP = str(headTOP[0]).lstrip()
						print (headTOP)

				elif line.startswith("ATTACHED_SITES_PATH:") :
						attacched_sites_path = re.findall(r"(?<=ATTACHED_SITES_PATH\:).*txt",line)
						attacched_sites_path = str(attacched_sites_path[0]).lstrip()
						attacched_sites = np.genfromtxt(attacched_sites_path, comments="#",dtype=np.int)


				elif line.startswith("INSERTION_METHOD:") :
					if re.match(r"(.*)(?<=)\s*manual", line):
						insertion_method = "manual"
						print ("manual insertion method loaded")

					elif re.match(r"(.*)(?<=)\s*automatic", line):
						insertion_method = "automatic"
						print ("automatic insertion method loaded")

				elif re.match(r"^\[[a][1-5]\]", line):

					if re.match(r"^\[[a][1]\]", line):
						smile_a1 = line

					elif re.match(r"^\[[a][2]\]", line):
						smile_a2 = line

					elif re.match(r"^\[[a][3]\]", line):
						smile_a3 = line

					elif re.match(r"^\[[a][4]\]", line):
						smile_a4 = line

					elif re.match(r"^\[[a][5]\]", line):
						smile_a5 = line


				elif re.match(r"^[a][1-5]\=", line):
					A1 = re.compile(r"^[a][1]\=(.*)")
					A2 = re.compile(r"^[a][2]\=(.*)")
					A3 = re.compile(r"^[a][3]\=(.*)")
					A4 = re.compile(r"^[a][4]\=(.*)")
					A5 = re.compile(r"^[a][5]\=(.*)")


					if re.match(A1, line):
						a1 = int(re.match(A1,line)[1].strip("%"))
						a1_p = a1

					elif re.match(A2, line):
						a2 = int(re.match(A2,line)[1].strip("%"))
						a2_p = a2

					elif re.match(A3, line):
						a3 = int(re.match(A3,line)[1].strip("%"))
						a3_p = a3

					elif re.match(A4, line):
						a4 = int(re.match(A4,line)[1].strip("%"))
						a4_p = a4

					elif re.match(A5, line):
						a5 = int(re.match(A5,line)[1].strip("%"))
						a5_p = a5


		if insertion_method == "automatic":
	
			TOT_p = a1_p + a2_p + a3_p + a4_p + a5_p

			if TOT_p != 100:
 				  raise RuntimeError("The total percentage is different then 100! "
										"Check your input file!")

			AtomSites = attacched_sites

			a1 = round ((a1* (len(AtomSites)) ) / 100)
			smile_a1 = [ smile_a1 ] * a1

			a2 = round ((a2* (len(AtomSites)) ) / 100)
			smile_a2 = [ smile_a2 ] * a2

			a3 = round ((a3* (len(AtomSites)) ) / 100)
			smile_a3 = [ smile_a3 ] * a3

			a4 = round ((a4* (len(AtomSites)) ) / 100)
			smile_a4 = [ smile_a4 ] * a4

			a5 = round ((a5* (len(AtomSites)) ) / 100)
			smile_a5 = [ smile_a5 ] * a5

			TOT = a1 + a2 + a3 + a4 + a5

			print ("a1="+str(a1_p)+"% ---> " + str(a1) + " chains"  )
			print ("a2="+str(a2_p)+"% ---> " + str(a2) + " chains"  )
			print ("a3="+str(a3_p)+"% ---> " + str(a3) + " chains"  )
			print ("a4="+str(a4_p)+"% ---> " + str(a4) + " chains"  )
			print ("a5="+str(a5_p)+"% ---> " + str(a5) + " chains"  )


			print ("The sum is " + str(TOT_p) + "% ---> " + str(TOT) + " chains"  )
			
			

			for idx,i in enumerate (AtomSites[0:a1]):
				SMILES_automatic.append(smile_a1[idx].replace("a1",str(i)))

			if a2 != 0 :
				if a3 == 0:
					for idx2,i2 in enumerate (AtomSites[a1:]):
						SMILES_automatic.append(smile_a2[idx2].replace("a2",str(i2)))
				
				elif a3 != 0:
					for idx2,i2 in enumerate (AtomSites[a1:a1+a2]):
						SMILES_automatic.append(smile_a2[idx2].replace("a2",str(i2)))
					
					if a4 == 0 :
					
						for idx3,i3 in enumerate (AtomSites[a1+a2:]):
							SMILES_automatic.append(smile_a3[idx3].replace("a3",str(i3)))
		

					if a4 != 0 :
						for idx3,i3 in enumerate (AtomSites[a1+a2:a1+a2+a3]):
							SMILES_automatic.append(smile_a3[idx3].replace("a3",str(i3)))
					
						if a5 == 0 :

							for idx4,i4 in enumerate (AtomSites[a1+a2+a3:]):
								SMILES_automatic.append(smile_a4[idx4].replace("a4",str(i4)))

						elif a5 != 0 :
							for idx4,i4 in enumerate (AtomSites[a1+a2+a3:a1+a2+a3+a4]):
								SMILES_automatic.append(smile_a4[idx4].replace("a4",str(i4)))

							for idx5,i5 in enumerate (AtomSites[a1+a2+a3+a4:]):
								SMILES_automatic.append(smile_a5[idx5].replace("a5",str(i5)))
			

			SMILES_automatic = [el.replace('\n', '') for el in SMILES_automatic]
			SMILES = []
			for item in SMILES_automatic:
				SMILES.append( NPbuilder.str2smiles(ff,item)[0] )


		else:
			SMILES = SMILES_manual

		SMILES = np.array(SMILES)	
		print (str(len(SMILES)) + " chains will be build")
		np.savetxt(outputDIR+"/"+"smiles.smi", SMILES, fmt="%s")


		AtomSites = np.array (AtomSites)	

		NPbuilder.builder(SMILES , shuffling, corePATH, 
						 AtomSites, ff, insertion_method, molName, headTOP, outputDIR, mode )


		return ff, shuffling, SMILES, AtomSites, insertion_method, molName, headTOP, outputDIR, mode



	def builder(smiles, shuffling, corePATH, AtomSites , forcefield, 
				InsertionMethod, molName, headTOP, outputDIR, mode):


		"""
        It is the actual builder of the final 3D Structure.
        If the ``shuffle`` option is set to ``yes``, the order of the SMILE strings will be randomized.
        
        It first loads the PDB template as an openbabel object and calculates the center of geometry of it.
        Secondly, it calls the ``common_tools.SMILEtoOBLIST`` function to convert the SMILE strings in openbabel objects.
        Each openbabel object (SMILE) is attached to the attaching-site of the template.
        A new bond is created between the template and SMILE.
        Finally, it generates bonds, pairs, angles, dihedrals list from the final 3D structure.

        ** The nanoparticle builder work in a slite different way then the ``lipid builder``:
        Since the nanoparticle 3D structure contains too many atoms for a performant minimization step with openbabel,
        each SMILE string is minimized before to be attached to the template file. The parameters (bonds, pairs, angles, and torsions)
        are calculated separately for each SMILE string and are renumerated after any cycle. **  

		"""

		start_time = time.time()    #timeit
		
		# quench output of non-conforming pdb-format
		ob.obErrorLog.SetOutputLevel(-1)
		

		## START Reading smile file with side chains:
		
		# Shufle position side chains if needed
		if shuffling == "yes":
			shuffle (smiles)
			np.savetxt(outputDIR+"/"+"smile_shuffled.smi", np.array(smiles), fmt='%s')
		else : pass
				
		## END      
	
		## Initiating OBmol
		
		## START Calculationing coordinates of Center of Geometry of core:
		
		OB_CORE = []


		OB_CORE = next(pybel.readfile("pdb", corePATH)).OBMol
		OB_CORE_coords = common_tools.getCoordinatesFROMobmol(OB_CORE)
		COG  = np.mean(OB_CORE_coords, axis=0)
		print("The Geometrical Center of the Nanoparticle is " + str(COG))
		#
		### END
		#

		## From smileStrings2OBlist:
		OB_LIST = common_tools.SMILEtoOBLIST(smiles,int(len(smiles)))
		
		if forcefield_dict[forcefield]  == "united_atom" :
			for i in OB_LIST:
				i.DeleteHydrogens()
		        
		#
		### Append side chains to OBmol
		#    
		## Starting the main loop over the side chains:
		nchains = len(smiles)
		conv = ob.OBConversion()
		Slist = AtomSites
		Slist = np.array(Slist)
		siteCoords = []
		for atom in ob.OBMolAtomIter(OB_CORE):
			if int(atom.GetId() +1) in  Slist :
				siteCoords.append((atom.GetX(),atom.GetY(),atom.GetZ()))
		siteCoords = np.array(siteCoords)
		SkeletronAtomNumbers = OB_CORE.NumAtoms()
		
		OB_CORE_NumAtoms = OB_CORE.NumAtoms()

		cn=0
		bonds            = []
		angles           = []
		angles_initial   = []
		torsions         = []
		pairs14          = []

		for chain in range(nchains):

			# Get x,y,z coordinates from OBMol Objects
			OB_coords = common_tools.getCoordinatesFROMobmol(OB_LIST[chain])
			#print (OB_coords)
			# Define the vector from COG to the attacched sites.
			siteVector = siteCoords[chain] - COG
			# Rotate side chain to match opitmal attaching direction -using rotational matrix
			# and translate side chain to a posisition close to the attaching site.
			common_tools.Rototranslation(OB_LIST[chain], siteVector, siteCoords[chain])
			# Delete the 1st atom if it is Sulfur:
			# The 1st atom is transformed always in a Sulfur atom 
			# to generate a straight chain.
			NPbuilder.delete1stIsSulfur(OB_LIST[chain])
			# Append to OBmol NP structure
			NumAtom = OB_CORE.NumAtoms()
			OB_CORE += OB_LIST[chain]
			# Add missing bond between : attacched site - side chain
			OB_CORE.AddBond(int(Slist[chain]),NumAtom+1,1)
			
			
			# Define Bonds
			connector = np.array([Slist[chain],1+NumAtom]).reshape(1,2)
			rest = (np.array([ i for i in common_tools.getBonds(OB_LIST[chain])]) + NumAtom)

			if rest.size == 0:
				bonds.append(bonds_chain)

			else:	
				bonds_chain = np.concatenate((connector,rest))
				bonds.append(bonds_chain)


			# Define angles
			angles_chain = np.array([ i for i in common_tools.getAngles(OB_LIST[chain])]) + NumAtom


			# Add angle between S and the first 3 atoms of each chain:
			angles_initial.append([Slist[chain], 1+NumAtom, 2+NumAtom])
			angles_initial.append([Slist[chain], 1+NumAtom, 3+NumAtom])
			angles_initial.append([Slist[chain], 1+NumAtom, 4+NumAtom])

			angles.append(np.vstack(np.array(angles_initial)))

			if np.array(angles_chain).size == 0:
				continue

			else:
				angles.append(angles_chain)

			# Define Dihedrals
			torsions_chain = np.array([ i for i in common_tools.getTorsions(OB_LIST[chain])]) + NumAtom
			
			if np.array(torsions_chain).size == 0:
				continue
			
			else:
				torsions.append(torsions_chain)


			# Get 1-4 pair list
			pairs14_chain = common_tools.getPairs(torsions_chain, angles_chain, bonds_chain)
			pairs14.append (pairs14_chain)
			
   

		conv.WriteFile(OB_CORE,outputDIR+"/"+str(molName)+".pdb")
		bonds    = np.concatenate(np.array(bonds))
		angles   = np.concatenate(np.array(angles))

		if np.array(torsions).size == 0:
			torsions = []
		else:
			torsions = np.concatenate(np.array(torsions))
		
		if np.array(pairs14).size == 0:
			pairs14 = []
		else:
			pairs14    = np.concatenate(np.array(pairs14))

		# return the lists
		parList = common_tools.genLists(bonds, pairs14, angles, torsions)		

		NumAtoms = OB_CORE.NumAtoms()

		if mode == "test":
			print ("Test mode selected, only 3D structure is generated")
			sys.exit(0)

		if forcefield == "GROMOS54a7":
			mode == "test"
			print ("Particle definitions for GROMOS54a7 are not provided yet.")
			print ("Only the 3D model will be built.")

		###### FORCEFIELD #####################################################

		if   forcefield == "CHARMM36":
			from forcefields.nanoparticle.charmm36.charmm36 import CHARMM36
			CHARMM36.nanoparticle_ff (smiles, SkeletronAtomNumbers, parList, 
									molName, headTOP, outputDIR, OB_CORE_NumAtoms)

		elif forcefield == "MARTINI":
			from forcefields.nanoparticle.martini.martini_v2_2 import MARTINI2_2
			MARTINI2_2.nanoparticle_ff (smiles, SkeletronAtomNumbers, parList, 
									molName, headTOP, outputDIR, OB_CORE_NumAtoms, NumAtoms)

		#####################################################################

		return print("--- %s seconds ---" % int((time.time() - start_time))) #print timeit


	def  str2smiles(forcefield, string):
			
			"""

			Here, the parser/lexer expressions are defined and heandler about the forcefield type.

			* In case of ``united atoms`` forcefield, the hydrogens are removed from the SMILE.
			* In case of ``Martini`` forcefield, ``4 TAIL units == 1 Martini bead``.

			Currently only three type of expressions are implemented (see ``lipid_builder.py`` for their implementation):

    		1. SITE SEPARATE HEAD e.g, ``[10]-CH3``
    		2. SITE SEPARATE MULTIPLE TAIL SEPARATE HEAD e.g, ``[10]-(15)CH2-CH3``
    		3. SITE SEPARATE MULTIPLE TAIL SEPARATE DOUBLEBOND SEPARATE MULTIPLE TAIL SEPARATE HEAD e.g, ``[10]-(7)CH2-cC=C-(7)CH2-CH3``.
			"""

			tokens = NPlexer.tokens
			
			# If Insertion Method is set to manual:
			def p_expression_1(p):
				"expression : SITE SEPARATE MULTIPL TAIL SEPARATE HEAD"

				# if N(CH3)3 Group is selected, adapt it to SMILE grammar
				if p[6] == str("mN(CH3)3"):
					p[6] = str("[CH2][N@SP3]([CH3])([CH3])([CH3])")


				elif p[6] == str("mCOO"):
					p[6] = str("[CH2][C@SP2]([O])([O])")

				elif p[6] == str("CH3"):
					p[6] = str("[CH3]")	

				elif p[6] == str("mSO4"):
					p[6] = str("[CH2][O]S(=O)(=O)[O]")	

				elif p[6] == str("mNH3"):
					p[6] = str("[CH2][NH3]")

				elif p[6] == str("mGLY"):
						p[6] = str("[CH2][C@SP3]([H])([OH])([C@SP3]([H])([H])([OH]))")


				p[0] = (p[1] + p[2] + str("[S]") + p[2] + (int((''.join(c for c in p[3] if c not in '()')))-1) * str("["+p[4]+"]"+p[5]) 
					+ str("["+p[4]+"]") + p[5] + str(p[6]))



			def p_expression_2(p):
				"expression : SITE SEPARATE HEAD"


				if p[3] == str("mN(CH3)3"):
					p[3] = str("[CH2][N@SP3]([CH3])([CH3])([CH3])")

				elif p[3] == str("mCOO"):
					p[3] = str("[CH2][C@SP2]([O])([O])")

				elif p[3] == str("CH3"):
					p[3] = str("[CH3]")	

				elif p[3] == str("mSO4"):
					p[3] = str("[CH2][O]S(=O)(=O)[O]")	

				elif p[3] == str("mNH3"):
					p[3] = str("[CH2][NH3]")

				elif p[3] == str("mGLY"):
						p[3] = str("[CH2][C@SP3]([H])([OH])([C@SP3]([H])([H])([OH]))")


				p[0] = (p[1] + p[2] + str("[S]") + p[2] + str(p[3]))


			def p_expression_3(p):
				"expression : SITE SEPARATE MULTIPL TAIL SEPARATE DOUBLEBOND SEPARATE MULTIPL TAIL SEPARATE HEAD"


				if p[6] == str("cC=C"):
					if forcefield_dict[forcefield] == "martini" :
						p[6] = str("[Al]")
					else:
						p[6] = str("/[CH]=[CH]\\")

					if p[11] == str("mN(CH3)3"):
						p[11] = str("[CH2][N@SP3]([CH3])([CH3])([CH3])")


					elif p[11] == str("mCOO"):
						p[11] = str("[CH2][C@SP2]([O])([O])")

					elif p[11] == str("CH3"):
						p[11] = str("[CH3]")	

					elif p[11] == str("mNH3"):
						p[11] = str("[CH2][NH3]")

					elif p[11] == str("mSO4"):
						p[11] = str("[CH2][O]S(=O)(=O)[O]")	

				elif p[11] == str("mGLY"):
						p[11] = str("[CH2][C@SP3]([H])([OH])([C@SP3]([H])([H])([OH]))")


				p[0] =  (p[1] + p[2] + str("[S]") + p[2]+ int(''.join(c for c in p[3] if c not in '()')) * str("["+p[4]+"]" 
						+ p[5]) + str(p[6]) + p[7]+
						+ int(''.join(c for c in p[8] if c not in '()')) * str("["+p[9]+"]" 
						+ p[10]) + p[11])



			# Error rule for syntax errors
			def p_error(p):
				print("Syntax error in input!")
			
			# Build the parser
			parser = yacc.yacc()			
			
			result = parser.parse(string)
			
			if forcefield_dict[forcefield] != "martini" :
				SiteNumber = (int (re.findall(r"\d+",result)[0]) )
				result = re.sub(r"^\[\d+\]\-", "", result) 


			elif forcefield_dict[forcefield]   == "martini":
				# Dictionary for the head groups:
				# each terminal group is named differently
				# if the atomtype is different in the Martini forcefield
				headGroups = {
				"[CH3]"   											: 	["[C]"],
				"[CH2][C@SP2]([O])([O])"  	 						: 	["[O]"],
				"[CH2][NH3]"   										: 	["[N]"],
				"[CH2][N@SP3]([CH3])([CH3])([CH3])" 				:	["[B]"],
				"[CH2][O]S(=O)(=O)[O]"              				:	["[O]"],
				"[CH2][C@SP3]([H])([OH])([C@SP3]([H])([H])([OH]))"  :	["[P]"],
				}


				# The SMILE string is split into single groups:
				# The SITE and the HEAD groups are removed from the counting.
				# The total number of groups is divided by 4 to follow the MARTINI 
				# rules for CH2 lipid tails.

				# The CG bead Number for the chains is rounded.
				SiteNumber = (int (re.findall(r"\d+",result)[0]) )
				result = re.sub(r"^\[\d+\]\-", "", result)
				split = result.split("-")
				
				if "[Al]" in split:
					for idx, i in enumerate(split):
						if i == "[Al]":
							beadsNumber_1  = round(((int(idx)-2)/4))
							tails = int(beadsNumber_1) * "[C] "
							tails += "[Al] " 
							beadsNumber_2  = round(((len(split)-1)-(int(idx)+1))/4)
							tails += int(beadsNumber_2) * "[C] "
							tails = "-".join(tails.split(" ")[:-1:])
							split[-1] = headGroups[split[-1]][0]
							newList = "-".join([split[0],  tails     ,split[-1]]) 
							newList = newList.replace("--","-")
							result  = newList 

				else:
						beadsNumber  = round(((len(split)-2)/4))
						tails = int(beadsNumber) * "[C] "
						tails = "-".join(tails.split(" ")[:-1:])
						split[-1] = headGroups[split[-1]][0]
						newList = "-".join([split[0],  tails     ,split[-1]]) 
						newList = newList.replace("--","-")
						result  = newList 
			return result, SiteNumber
		
	

	def SmileList2Lists(smilesList):
		"""
		It reads a SMILE list and convert it in  a list of len(smiles) lists.
		Furtheremore, It replaces the attched site number [ATTACHED_SITE] to [S] and
		Convert back the new list of len(smiles) lists to 1 list of 
		len(smile) elements.

		"""
	
		splitted = []
		for line in smilesList:
			splitted.append(line.split("-"))
		smileLists = []
		NewSmiles  = []
		for List in range(len(splitted)):
			smileLists.append ( [item.replace('[ATTACCHED_SITE]', '[S]') for item in splitted[List]] )
			NewSmiles.append ("-".join(smileLists[List])) 	
		return NewSmiles, smileLists
	
	
	def delete1stIsSulfur(OBMol_Object):

		"""
		It deletes the first atom of the SMILES string. It is always a sulphur atom due to the substitution made by the ``SmileList2Lists`` function.
		"""

		import openbabel
		import pybel
		
		for atom in ob.OBMolAtomIter(OBMol_Object):
			if atom.GetIdx() == 1:
				if atom.IsSulfur():
					OBMol_Object.DeleteAtom(atom)



	def TopolReader(inputFN):

		"""
		It walks on the final 3D structure and perception the bonds, pairs, angles, and dihedral lists.
		"""

		core_atoms     = []
		core_bonds     = []
		core_pairs     = []
		core_angles    = [] 
		core_dihedrals = []
		core_impropers = []
		
		with open(inputFN,"r") as file:
			for line in file:
				if line.startswith("#"):
					continue
				if line.startswith("\n"):
					continue
				elif "ATOMS" in line:
					break
		
		
			for line in file:
				if line.startswith("#"):
					continue
				if line.startswith("\n"):
					continue
				elif "BONDS" in line:
					break
				else:
					core_atoms.append(line)
		
		
			for line in file:
				if line.startswith("#"):
					continue
				if line.startswith("\n"):
					continue
				elif "PAIRS" in line:
					break
				else:
					core_bonds.append(line)
		
		
			for line in file:
				if line.startswith("#"):
					continue
				if line.startswith("\n"):
					continue
				elif "ANGLES" in line:
					break
				else:
					core_pairs.append(line)
		
		
			for line in file:
				if line.startswith("#"):
					continue
				if line.startswith("\n"):
					continue
				elif "DIHEDRALS" in line:
					break
				else:
					core_angles.append(line)
		
		
		
			for line in file:
				if line.startswith("#"):
					continue
				if line.startswith("\n"):
					continue
				elif "IMPROPERS" in line:
					break
				else:
					core_dihedrals.append(line)
		
		
		
			for line in file:
				if line.startswith("#"):
					continue
				if line.startswith("\n"):
					continue
				else:
					core_impropers.append(line)
		
		

		if np.array(core_atoms).size:
			# atom number, atom type, atom name, charge, mass
			core_atoms     = np.stack(np.core.defchararray.split(core_atoms    ))

		if np.array(core_bonds).size:
			core_bonds     = np.stack(np.core.defchararray.split(core_bonds    ))

		if np.array(core_pairs).size:
			core_pairs     = np.stack(np.core.defchararray.split(core_pairs    ))

		if np.array(core_angles).size:
			core_angles     = np.stack(np.core.defchararray.split(core_angles    ))

		if np.array(core_dihedrals).size:
			core_dihedrals     = np.stack(np.core.defchararray.split(core_dihedrals   ))

		if np.array(core_impropers).size:
			core_impropers     = np.stack(np.core.defchararray.split(core_impropers    ))


		return core_atoms, core_bonds, core_pairs, core_angles, core_dihedrals, core_impropers