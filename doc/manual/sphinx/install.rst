=============================
Installation
=============================

``OpenBuilder`` require python >=3.6

The dependencies can be installed by using the following methods:

Through Anaconda Cloud
---------------------------

Anaconda can be downloaded directly from its web page:
www.continuum.io/downloads

After the installation of Anaconda, it is required to install the below dependencies:

>>>
conda install -c openbabel openbabel=2.4.1
conda install numpy=1.11.3
conda install ply
conda install -c omnia parmed 
conda install -c omnia openmm
git clone https://github.com/shirtsgroup/InterMol.git
cd InterMol
pip install .


Manual Installation
---------------------------

For manual installation, the users are required to install the following dependencies to run
OpenBuilder tool. We expect the users to have python 3 installed (the OpenBuilder tool was tested with
python 3.6).

>>>
openbabel 2.4.1 or greater
    -libpng-1.6.28 or greater
    -pixman-0.34.0 or greater
    -cairo-1.14.8 or greater
    -zlib-1.2.11 or greater
    -swig-3.0.12 or greater
    -Eigen 3.3.3 or greater
Numpy-1.11.3
PLY
OpenMM
Parmed
InterMol
