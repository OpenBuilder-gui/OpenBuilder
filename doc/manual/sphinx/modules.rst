=============================
How to Create a New Module
=============================

The power of ``OpenBuilder`` is in its flexibility.
Each module is indipendent from the others. 
Althought, they  shares the same workflow and input file:
``Openbabel`` is the builder engine and is uses the ``SMILE`` format for describing/building the molecules.
The modules are mede up by 3 python files:

* A parser/lexer (parslexer.py),
* The molecule builder (builder.py),
* The forcefield generator (forcefield.py).

The Parser/Lexer (parslexer/module.py)
---------------------------------------

The parser/lexer is handled by ``PLY`` library.
``PLY`` is an implementation of ``lex`` and ``yacc`` parsing tools for Python.
It is ,mainly, madeup by two parts.
The first one describs the lexical of the input file and it is incorporated inside the ``parslexer.py``.
The idea of the `lexer` is to write input file easy to write from the user side.
The second part is defined into the ``builder.py`` file and it translates the input file to SMILE string readble by `openbabel`.

For example: In the ``Lipid Builder`` the string ``[10]-(7)CH2-mNH3`` in the input file is translated as:

>>> [X]-(7)CH2-CH3 :
[X]-[CH2]-[CH2]-[CH2]-[CH2]-[CH2]-[CH2]-[CH2]-[CH3]

Where ``X`` is the serial number of the atom in  ``template.pdb`` where the SMILE chain will be attached.

In this example the expression is divided into ``tokens``.

* ``[X]``  = ``SITE``
* ``-`` = ``SEPARATE``
* ``(7)`` = ``MULTIPL``
* ``[CH2]`` = ``TAIL``
* ``[CH3]`` = ``HEAD``


``SITE``, ``MULTIPL``, ``TAIL``, ``HEAD`` are the `tokens` and they are described inside the ``parslexer.py``.


>>> parlexer/lipid.py :
tokens = (
       'SITE',    
       'MULTIPL',
       'TAIL',
       'DOUBLEBOND',
       'HEAD',
       'SEPARATE',
    )
    # Regular expression rules for simple tokens
    t_SEPARATE    =     r'-'
    # t_SITE match "[anynumber]" at the beginning of the string
    t_SITE       =  r"^\[[1-9][0-9]*\]"
    # t_MULTIPL match "(anynumber)"
    t_MULTIPL     =     r"\([0-9][0-9]*\)"
    t_TAIL        =     (
                            r"CH2"                            
                        )
    t_DOUBLEBOND   =     (
	                      r"cC=C"                  
	                       )
    # t_MULTIPL match one of those groups at the end of the string
    t_HEAD        =     (
                        r"CH3$"        # Methyl Group      ->  CH3
                        )
 

The allow expressions are described in the ``pytools/module_builder.py`` file.




The Builder (pytools/module_builder.py)
-----------------------------------------

The ``module_builder.py`` is the actual builder. It interpretes the input file and convert it in SMILE strings readble by openbabel. It builds the molecules and attach them to the template PDB file.
The most crucial functions in the ``module_builder.py`` are:

* ``inputReader()`` :


In the ``inputReader()`` are defined which parameters have to be taken from the input file and how they have to be interpreted.

For example in the ``Nanoparticle Builder`` the ``shuffle`` option can be defined in the input file:

>>>
elif line.startswith("Shuffle:") :
if re.match(r"(.*)(?<=)\s*yes", line):
	shuffling = "yes"
	print ("Shuffle ON:\nThe chains will be pseudo-randomly placed on the indicated attasites.")
elif re.match(r"(.*)(?<=)\s*no", line):
	shuffling = "no"
	print ("Shuffle OFF")

It reads the input file and search for a line starting with: ``Shuffle:``.
It can be followed by a ``yes`` or ``not``. Based on the choice a different action is taken.

* ``str2smiles()`` : 

The ``str2smiles()`` function defines how the expressions have to built and understood. It is the ``parser``.
For example in the ``Nanoparticle Builder`` three different expressions are allowed:

1. SITE SEPARATE HEAD e.g, ``[10]-CH3``
2. SITE SEPARATE MULTIPLE TAIL SEPARATE HEAD e.g, ``[10]-(15)CH2-CH3``
3. SITE SEPARATE MULTIPLE TAIL SEPARATE DOUBLEBOND SEPARATE MULTIPLE TAIL SEPARATE HEAD e.g, ``[10]-(7)CH2-cC=C-(7)CH2-CH3``.

Where :

* ``SITE``
* ``SEPARATE``
* ``MULTIPL``
* ``DOUBLEBOND``
* ``TAIL``
* ``HEAD``

are the `tokens` defined in ``parlexer/lipid.py``. 


* ``builder()``: It is the actual builder.

It convert the SMILE string in openbabel object (3D molecule) and create the bonds with the ``template.pdb`` after a rototranslation.
In the currently modules, each molecule is rototranslated based on the vector between the `center of geometry` of the template molecule and the attaching site where the SMILE molecule has to be linked.
Openbabel migh be used to minimize the structure after every chain is added or when all the chains are added.

When the final structure is made openbabel is used to perceive the bonds, pairs, angles, and dihedral lists.



The Forcefield (forcefields/module/forcefield/forcefield.py)
------------------------------------------------------------

The ``forcefields/module/forcefield/`` contains two files:

* ``forcefield.py`` contains the forcefield parameters for the building block defined in the parser/lexer section.
* ``forcefield.itp`` or  ``forcefield.ff`` (folder) contains the ctual e.g., CHARMM36, MARTINI or OPLS forcefield parameters files for GROMACS.


Each item in the ``TAIL`` and ```DOUBLEBOND` and ``HEAD`` tokens are here described as ``[ moleculetype ]`` information for GROMACS.

>>>
# [CH2] = CH2
CH2 = np.array(["CTL2",	molName	,	"C@",	"-0.180",     "12.0110",   
				"HAL2",	molName	,	"H@",	"0.090" ,     "1.0080",    
				"HAL2",	molName	,	"H@",	"0.090" ,     "1.0080"]).reshape(3,5) 
# [CH3] = CH3
CH3 = np.array(["CTL3",molName,	"C@"	,	"-0.270",    "12.0110",   
				"HAL3",molName,	"H@"	,	"0.090" ,     "1.0080",    
				"HAL3",molName,	"H@"	,	"0.090" ,     "1.0080",
				"HAL3",molName,	"H@"	,	"0.090" ,     "1.0080"]).reshape(4,5)


Assuming that the input file contains this string:
``[10]-(7)CH2-CH3`` it will be converted by the ``pytools/module_builder.py`` in ``[10]-[CH2]-[CH2]-[CH2]-[CH2]-[CH2]-[CH2]-[CH2]-[CH3]`` 

Now, the ``forcefields/module/forcefield/forcefield.py`` split this string by using ``-`` as splitting motif.
The abow chains will be splitted in 9 items:

* [10]  -> Here are stored the information regarding where the chains has to be attached. 
* [CH2] -> forcefield.py add the above molecule type information for ``CH2`` in the GROMACS ``molecule.itp`` file.
* [CH2] -> \\ As above \\
* [CH2] -> \\ As above \\
* [CH2] -> \\ As above \\
* [CH2] -> \\ As above \\
* [CH2] -> \\ As above \\
* [CH2] -> \\ As above \\
* [CH3] -> forcefield.py add the above molecule type information for ``CH3`` in the GROMACS ``molecule.itp`` file.



When a [CH2] is read from the ``forcefield.py``, it will change (e.g., in case of ``Lipid Builder`` module) the initial description ``C@`` or ``H@`` stored in the CH2 variable, based on the lipid tail we are considering (``SN1`` or ``SN2``) in this way:

>>>
[...]
elif Groups[splitted_smiles[i][j]][0]	== "CH2":
	cnt += 3
	# Reset the original values
	CH2_copy = np.copy(CH2)
	# Modify the copied values
	if i == 0:
		CH2_copy[0][2] = "C"+str(i+2)+str(j+2)
		CH2_copy[1][2] = "H"+str(j+2)+"R"
		CH2_copy[2][2] = "H"+str(j+2)+"S"
	elif i == 1:
		CH2_copy[0][2] = "C"+str(i+2)+str(j+2)
		CH2_copy[1][2] = "H"+str(j+2)+"X"
		CH2_copy[2][2] = "H"+str(j+2)+"Y"
	ff_temp.append(CH2_copy)

It also updates the ``cnt`` in case is needed to add some improper dihedrals.
For example, in case of ``OPLSAA`` forcefield ``Lipid Builder`` module, an improper dihedral for the cis double bond has to be manually added each time.

>>>
[...]
elif Groups[splitted_smiles[i][j]][0]	== "cC=C":
[...]
	# Store improper dihedrals
	cisCC_impropers = (np.array([
				[cnt-6, cnt-3, cnt-1, cnt+1, 2 , "0", "167.4"],
				[cnt-3, cnt-6, cnt-2, cnt-1, 2 , "0", "167.4"],
				[cnt-1, cnt-3, cnt  , cnt+1, 2 , "0", "167.4"]]
				))
	# Append improper dihedrals to the impropers list
	# If it is a list, it is a empty list
	if str(type(coreTOP[4])) == "<class 'list'>":
		impropers = cisCC_impropers
	# Otherwise it is a non empty numpy array
	else:
		impropers = np.append(impropers, cisCC_impropers, axis=0)


At the end of the reading a ``molecule.itp`` is written as text file.


The `include` Folder
------------------------------------------------------------

In the `include` folder the templates are stored.

Each modules can have different type of files.

* The ``Lipid Builder`` needs only two files:

1. A ``template.pdb`` file for the 3D Structure
2. A ``template.top`` for the molecule types, bonds, angle, and dihedral parameters of the template.

>>>  Inside lipid_input.txt you can find:
PDB_INPUT_PATH:  ./include/lipids/OPLSAA2/PC/PC.pdb
TOP_INPUT_PATH:  ./include/lipids/OPLSAA2/PC/PC.top 

* The ``Nanoparticle Builder`` needs three files:

1. A ``template.pdb`` file for the 3D Structure
2. A ``template.top`` for the molecule types, bonds, angle, and dihedral parameters of the template.
3. A ``attached_sites.txt`` file where the information about the availbale attaching sites (serial atom numbers) are stored.


>>>  Inside nanoparticle_input.txt you can find:
PDB_INPUT_PATH:      ./include/nanoparticles/Au144_RS_60/skeleton.pdb
TOP_INPUT_PATH:      ./include/nanoparticles/Au144_RS_60/NP.top
ATTACHED_SITES_PATH: ./include/nanoparticles/Au144_RS_60/attached_sites.txt



Final Remarks
------------------------------

When your module is ready remember to add an entry in the ``builder.py`` of the root folder to redirect to your new module (e.g, ``NEW_MODULE``):

>>> OpenBuilder/build.py
[...]
elif re.match(r"(.*) (?<=)\s*NEW_MODULE", line):
	from pytools.lipid_builder import NEW_MODULE
	print ("NEW_MODULE loaded")
	outputDIR = os.path.abspath(outputDIR)
	print ("Files will be created in "+ str(outputDIR))
	outputDIR = outputDIR+"/ultimate_builder"
	if os.path.isdir(outputDIR):
		shutil.rmtree (outputDIR)
		os.mkdir (outputDIR)
		print ("Directory already exist: overwrite")
	else:
		os.mkdir (outputDIR)
	print (outputDIR)
	options = NEW_MODULE.inputReader(inputFile, outputDIR)
	ff = options[0]
else : print("Module not found")
[...]