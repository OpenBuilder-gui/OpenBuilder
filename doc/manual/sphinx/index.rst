.. Openbuilder documentation master file, created by
   sphinx-quickstart on Thu Jun 28 15:51:00 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Openbuilder's documentation!
=======================================


.. toctree::
   :maxdepth: 2

   install
   how_to_use.rst
   code
   modules

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
