=============================
How to use
=============================


``python builder.py -i input.txt -o output_directory``


>>> python builder.py -h
usage: Builder [-h] [--version] [-i INPUTFILE] [-o OUTPUTDIR]
Molecule Builder
optional arguments:
  -h, --help            show this help message and exit
  --version             show program's version number and exit
  -i INPUTFILE, --input INPUTFILE
  -o OUTPUTDIR, --output OUTPUTDIR

