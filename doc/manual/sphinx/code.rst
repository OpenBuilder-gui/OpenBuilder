=============================
OpenBuilder Reference Manual
=============================

Common Reference
=================

It contains the common functions shared within all modules.

builder.py
--------------

.. automodule:: builder
   :members:

common.py
--------------

.. automodule:: pytools.common
   :members:


Lipid Builder Module
============================

The lipid builder module is made up of 3 separate python files:

* A parser/lexer to interpret/translate the user input file.
* The actual builder ``lipid_builder.py``: First, it converts the user input file in SMILE strings. The SMILE strings are converted in 3D molecules and attached to the template file found in the ``include`` folder. Finally, It makes the bonds, pair, angles and torsions lists for the full molecule.
* The forcefield generator fills up the missing ``[ moleculetype ]`` section fo the GROMACS topology by adding the atom types and the partial charges based on the selected forcefield.


Include
--------------

The ``include`` folder contains the template files used by the builder to attach the SMILE molecules to the template structure (``.pdb``) and generate the final topology.
Each forcefield contains its own template file:


============  ========== 
**CHARMM36**  **OPLSAA** 
============  ========== 
PC              PC  
TG              PE  
\-              PS   
\-              SPC   
============  ==========

* PC: The Glycero-3-phosphocholine template
* PE: The Glycero-3-phosphoethanolamine template
* PS: The Glycero-3-phosphoserine template
* SPC: The Sphingomyelin (choline) template
* TG: The Triglyceride template


Each Template contains two files:

* The 3D structure of the template in ``.pdb`` format.
* The ``[ moleculetypes ]`` GROMACS like section for the template. It contains the atom type, partial charges and improper dihedrals of the template.

>>>
ATOMS
#
#nr	type  	atom 	 charge  	mass
#
1        NTL          N       -0.600    14.0070
2       CTL5        C13       -0.350    12.0110
3         HL       H13A        0.250     1.0080
4        OSL        O21       -0.490    15.9994
#
#
BONDS
# 
#ai	aj	funct	b0	Kb
#
1 	2 	1
#
#
PAIRS
# 
#ai	aj	funct	c6	c12
#
1 	2 	1	
#
#
ANGLES
# 
#ai	aj	ak	funct	th0	cth	S0	Kub
#
1 	2 	3 	5	
#
#
DIHEDRALS
# 
#ai	aj	ak	al	funct	phi0	cp	mult
#
1 	2	 3	 4	 9
#
#
IMPROPERS
# 
#ai	aj	ak	al	funct	phi0	cp	mult
#

If there are particular/unusual bonds/pairs/angles/dihedrals/impropers, they can be added to their own section.


Parser/Lexer
--------------

.. automodule:: parslexer.lipid
   :members:


Builder
--------------

.. automodule:: pytools.lipid_builder
   :members:


Forcefields
--------------

CHARMM36
--------------

.. automodule:: forcefields.lipid.charmm36.charmm36
   :members:


OPLS-AA 2
--------------

.. automodule:: forcefields.lipid.oplsaa2.oplsaa2
   :members:



Input File
--------------
It is an example of input file for the ``Lipid Builder`` module:

>>>
# Specify which module you would like to load:
Module: lipid_builder
# Specify the path pf the template files:
PDB_INPUT_PATH:  ./include/lipids/OPLSAA2/PC/PC.pdb
TOP_INPUT_PATH:  ./include/lipids/OPLSAA2/PC/PC.top
# Specify the name of your molecule:
MOL_NAME: POPC
# Specify the forcefield you would like to use:
Forcefield: OPLSAA2
# Specify the chain type you would like to build:
# SN1:
[33]-(6)CH2-cC=C-(7)CH2-CH3
# SN2:
[42]-(13)CH2-CH3






Nanoparticle Builder Module
============================


The lipid builder module is made up of 3 separate python files:

* A parser/lexer to interpret/translate the user input file.
* The actual builder ``lipid_builder.py``: First, it converts the user input file in SMILE strings. The SMILE strings are converted in 3D molecules and attached to the template file found in the ``include`` folder. Finally, It makes the bonds, pair, angles and torsions lists for the full molecule.
* The forcefield generator fills up the missing ``[ moleculetype ]`` section fo the GROMACS topology by adding the atom types and the partial charges based on the selected forcefield.


Include
--------------

The ``include`` folder contains the template files used by the builder to attach the SMILE molecules to the template structure (``.pdb``) and generate the final topology.
Each forcefield contains its own template file:


====================   ====================
**CHARMM36**  		   **MARTINI** 
====================   ====================
Au144_RS_60            Au144_RS_60
====================   ====================

* Currently only the template based on the Au144(RS)60 cluster developped by Lolicato et al. is available.


Each Template contains three files:

* The 3D structure of the template in ``.pdb`` format.
* The ``[ moleculetypes ]`` GROMACS like section for the template. It contains the atom type, partial charges and improper dihedrals of the template.
* The ``attached_sites.txt``: A simple text file contained the list of the atoms number where the chains have to be built.

>>> An example of NP.top file:
ATOMS
#
#nr	type  	atom 	 charge  	mass
#
1        NTL          N       -0.600    14.0070
2       CTL5        C13       -0.350    12.0110
3         HL       H13A        0.250     1.0080
4        OSL        O21       -0.490    15.9994
#
#
BONDS
# 
#ai	aj	funct	b0	Kb
#
1 	2 	1
#
#
PAIRS
# 
#ai	aj	funct	c6	c12
#
1 	2 	1	
#
#
ANGLES
# 
#ai	aj	ak	funct	th0	cth	S0	Kub
#
1 	2 	3 	5	
#
#
DIHEDRALS
# 
#ai	aj	ak	al	funct	phi0	cp	mult
#
1 	2	 3	 4	 9
#
#
IMPROPERS
# 
#ai	aj	ak	al	funct	phi0	cp	mult
#

If there are particular/unusual bonds/pairs/angles/dihedrals/impropers, they can be added to their own section.


Parser/Lexer
--------------

.. automodule:: parslexer.nanoparticle
   :members:


Builder
--------------

.. automodule:: pytools.nanoparticle_builder
   :members:


Forcefields
--------------


CHARMM36
--------------

.. automodule:: forcefields.nanoparticle.charmm36.charmm36
   :members:


MARTINI 2.2
--------------

.. automodule:: forcefields.nanoparticle.martini.martini_v2_2
   :members:


Input File
--------------
It is an example of input file for the ``Nanoparticle Builder`` module:

>>>
# Specify which module you would like to load:
Module: NPbuilder
# Specify the path pf the template files:
PDB_INPUT_PATH:      ./include/nanoparticles/Au144_RS_60/skeleton.pdb
TOP_INPUT_PATH:      ./include/nanoparticles/Au144_RS_60/NP.top
ATTACHED_SITES_PATH: ./include/nanoparticles/Au144_RS_60/attached_sites.txt
# Specify the name of your molecule:
MOL_NAME: NP60
# Specify the forcefield you would like to use:
Forcefield: CHARMM36
# Do you want shuffle the chains?
Shuffle: no
# How many types of chain do you want build?
a1=20%
a2=20%
a3=20%
a4=20%
a5=20%
# Which kind of chains do you want build?
[a1]-(10)CH2-mN(CH3)3
[a2]-(10)CH2-mCOO
[a3]-(10)CH2-mGLY
[a4]-(5)CH2-cC=C-(5)CH2-mNH3
[a5]-CH3


