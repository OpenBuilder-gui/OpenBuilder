"""
The parser/lexer is made by the ``ply`` library only.
Here, only the parser is explained. The lexer part is explained in the ``lipid_builder`` section.
"""
import ply.lex as lex

class lipid_lexer(object):
    """
    Here, the token and basic rules of the parser/lexer are defined.
    The Tokens are divided into 6 groups:
    
    * ``SITE`` -> ``[X]``. To specify in which atom of the template the SMILE string have to be attached (e.g, ``[10]``).
    * ``MULTIPPLE`` -> ``(Y)``. It has to  be connected to `TAIL`. It specifies the number of repetition of the unit tail (e.g, ``(7)CH2``).
    * ``TAIL`` -> ``CH2``. Currently only the ``CH2`` unit is available as `TAIL`.
    * ``DOUBLEBOND`` -> ``cC=C``. Currently only the ``cC=C`` (cis C=C) unit is available as `DOUBLEBOND`. 
    * ``HEAD`` -> ``CH3``. Currently only the ``CH3`` unit is available as `HEAD`.
    * ``SEPARATE`` -> ``-``. It separates the tokens inside the expression.

    Currently only three type of expressions are implemented (see ``lipid_builder.py`` for their implementation):

    1. SITE SEPARATE HEAD e.g, ``[10]-CH3``
    2. SITE SEPARATE MULTIPLE TAIL SEPARATE HEAD e.g, ``[10]-(15)CH2-CH3``
    3. SITE SEPARATE MULTIPLE TAIL SEPARATE DOUBLEBOND SEPARATE MULTIPLE TAIL SEPARATE HEAD e.g, ``[10]-(7)CH2-cC=C-(7)CH2-CH3``.
    """

    tokens = (
       'SITE',    
       'MULTIPL',
       'TAIL',
       'DOUBLEBOND',
       'HEAD',
       'SEPARATE',
    )

    # Regular expression rules for simple tokens
    t_SEPARATE    =     r'-'
    # t_SITE match "[anynumber]" at the beginning of the string
    t_SITE       =  r"^\[[1-9][0-9]*\]"
                                       

    # t_MULTIPL match "(anynumber)"
    t_MULTIPL     =     r"\([0-9][0-9]*\)"


    t_TAIL        =     (
                            r"CH2"                            
                        )

    t_DOUBLEBOND   =     (
                            r"cC=C"                  
                        )



    # t_MULTIPL match one of those groups at the end pf the string
    t_HEAD        =     (
                        r"CH3$"        # Methyl Group      ->  CH3
                        )

   
    # Define a rule so we can track line numbers


    def t_newline(self,t):
        r"\n+"
        t.lexer.lineno += len(t.value)
        

    # A string containing ignored characters (spaces and tabs)
    t_ignore  = ' \t'

    # Error handling rule
    def t_error(self,t):
        """
        It prints an error message if a not defined token is selected.
        """
        print("Illegal character '%s'" % t.value[0])
        t.lexer.skip(1)

    # Build the lexer
    def build(self,**kwargs):
        self.lexer = lex.lex(module=self, **kwargs)
    
    # Test it output
    def test(self,data):
        self.lexer.input(data)
        while True:
             tok = self.lexer.token()
             if not tok: 
                 break
             print(tok)

# Build the lexer and try it out
m = lipid_lexer()
m.build()           # Build the lexer
#m.test("[200]-CH2-(10)CH2-CH3")     # Test it