"""
``builder.py`` is the main reader.
It checks the user input file and call the choosen module.
The main used Libraries are: \n

* ``argparse`` is used for the command-line input definition.
* ``sys`` is used for exit the code in case of missing/wrong input parameters.
* ``os`` allow the using of operating system dependent functionality.
* ``shutil`` is used for supporting file copying and removal.
* ``re`` is providing regular expression matching operations.


"""
import numpy as np
import argparse
import sys
import os
import shutil
import re
from pytools.common import common_tools


def optP():

	""" 
	Parse command line options: \n
	* ``--version`` prints the current version of the software.
	* ``-i, --input`` is used to specify the input text file (.txt)
	* ``-o, --output`` to specify the directory for the output. OpenBuilder will create (overwrite if already existing) a folder named ``ultimate_builder`` in the choosen directory.
	* ``-h, --help`` prints helpful information
	"""
	parser = argparse.ArgumentParser(description="Molecule Builder", prog='Builder')
	parser.add_argument('--version', action='version', version='%(prog)s 0.1')
	parser.add_argument("-i","--input", dest="inputFile", type=str)
	parser.add_argument("-o","--output", dest="outputDIR", type=str)
	
	
	
	args = parser.parse_args()
	

	if not os.path.isfile(args.inputFile):
		print("Error: File input is missing: {0}".format(args.inputFile))
		parser.print_usage()
		sys.exit(2)
	
	if not args.outputDIR:
		print("Error: Output directory is missing: {0}".format(args.outputDIR))
		parser.print_usage()
		sys.exit(2)
	
	return args

def reader(inputFile,outputDIR):

	""" 
	It reads the user input file and search for the ``Module`` section.
	It redirects the all software to the specific choosen module.
	Each string starting with ``#`` will be ignored.
	"""
	
	with open (inputFile,"r") as input:
		for line in input:
			if line.startswith("#") : pass
		
			elif line.startswith("Module: ") :
	
				if re.match(r"(.*) (?<=)\s*NPbuilder", line):
					#from bin.builders import NPbuilder_bin
					from pytools.nanoparticle_builder import NPbuilder
					print ("NPbuilder loaded")
					outputDIR = os.path.abspath(outputDIR)
					print ("Files will be created in "+ str(outputDIR))
					outputDIR = outputDIR+"/ultimate_builder"
	
					if os.path.isdir(outputDIR):
						shutil.rmtree (outputDIR)
						os.mkdir (outputDIR)
						print ("Directory already exist: overwrite")
	
					else:
						os.mkdir (outputDIR)
	
					print (outputDIR)
	
					options = NPbuilder.inputReader(inputFile, outputDIR)
					ff = options[0]
					shuffl = options[1]
	
				elif re.match(r"(.*) (?<=)\s*lipid_builder", line):
					from pytools.lipid_builder import lipid_builder
					print ("lipid_builder loaded")
					outputDIR = os.path.abspath(outputDIR)
					print ("Files will be created in "+ str(outputDIR))
					outputDIR = outputDIR+"/ultimate_builder"
	
					if os.path.isdir(outputDIR):
						shutil.rmtree (outputDIR)
						os.mkdir (outputDIR)
						print ("Directory already exist: overwrite")
	
					else:
						os.mkdir (outputDIR)
	
					print (outputDIR)
					options = lipid_builder.inputReader(inputFile, outputDIR)
					ff = options[0]
	
				elif re.match(r"(.*) (?<=)\s*surface_builder", line):
					print ("Sorry, not yet implemented!")
	
				else : print("Module not found")

if __name__ == '__main__':
	options = optP()
	reader(options.inputFile, options.outputDIR)