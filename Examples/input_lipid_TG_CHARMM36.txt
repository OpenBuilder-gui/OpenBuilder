# python build.py -i input_lipid_CHARMM36.txt -o ./
# https://gitlab.com/OpenBuilder-gui/OpenBuilder

Module: lipid_builder

PDB_INPUT_PATH:  ./include/lipids/CHARMM36/TG/TG.pdb
TOP_INPUT_PATH:  ./include/lipids/CHARMM36/TG/TG.top 

MOL_NAME: TG

Forcefield: CHARMM36


#[27]-(6)CH2-cC=C-(7)CH2-CH3

[24]-(13)CH2-CH3
[10]-(13)CH2-CH3
[19]-(13)CH2-CH3

