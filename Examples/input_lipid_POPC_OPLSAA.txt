Module: lipid_builder

PDB_INPUT_PATH:  ./include/lipids/OPLSAA2/PC/PC.pdb
TOP_INPUT_PATH:  ./include/lipids/OPLSAA2/PC/PC.top 

MOL_NAME: POPC

Forcefield: OPLSAA2


[33]-(6)CH2-cC=C-(7)CH2-CH3
[42]-(13)CH2-CH3

